Pour avoir le nom d'application en plusieurs langues, je n'ai pas trouvé de solution native ionic.
Donc on modifie les fichiers de build android directement. 
Il suffit de copier coller les dossiers values-xx dans platforms\android\app\src\main\res

Remarque: il faut une solution similaire avec pour IOS. (https://www.appcelerator.com/blog/2012/02/internationalization-of-app-names/)