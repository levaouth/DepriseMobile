import { Injectable, ElementRef } from '@angular/core';
import { Gesture } from 'ionic-angular';
/*
  Generated class for the SwipeFeedbackProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

declare var Hammer: any;

export interface positionCircleFeedback {
  x: number,
  y: number
}

@Injectable()
export class SwipeFeedbackProvider {

  constructor() {
    console.log('Hello SwipeFeedbackProvider Provider');
  }

  screenWidth: number;
  screenHeight: number;

  private canvas: HTMLCanvasElement;
  private context;
  private positions : positionCircleFeedback[] = [];
  private motionTrailLength : number = 10;
  private xPos : number;
  private yPos : number;
  public feedbackGesture : Gesture;

  initFeedback(canvas: HTMLCanvasElement, cadre : ElementRef){
    this.canvas = canvas;
    canvas.width = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight ;
    this.context = canvas.getContext("2d");

    this.feedbackGesture = new Gesture(cadre.nativeElement,  {
      recognizers: [
        [Hammer.Pan, {direction: Hammer.DIRECTION_ALL}],
      ]
    });
    this.feedbackGesture.listen();
  }

  stopFeedback(){
    this.feedbackGesture.off('pan', (event) => {
      this.feedBackupdate(event);
    });

    this.feedbackGesture.off('panend', (event) => {
      this.clearCanvas();
    });
    this.clearCanvas();
    // this.canvas.removeEventListener("touchmove",(e) => this.feedBackupdate(e));
    // this.canvas.removeEventListener("touchend",() => this.clearCanvas());
  }

  startFeedback(){
    this.feedbackGesture.on('pan', (event) => {
      this.feedBackupdate(event);
    });

    this.feedbackGesture.on('panend', (event) => {
      this.clearCanvas();
    });
    // this.canvas.addEventListener("touchmove",(e) => this.feedBackupdate(e));
    // this.canvas.addEventListener("touchend",() => this.clearCanvas());
  }

  endFeedback(){
    if(this.context != null)
      this.clearCanvas();
    if(this.feedbackGesture != null)
      this.feedbackGesture.destroy();
  }

  private feedBackupdate(event){
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    // let canvaPosition = this.canvas.getBoundingClientRect();
    this.xPos = Math.floor(event.center.x);
    this.yPos = Math.floor(event.center.y);
   
    for (var i = 0; i < this.positions.length; i++) {
      var ratio = (i + 1) / this.positions.length;
      this.drawCircle(this.positions[i].x, this.positions[i].y, ratio);
    }
   
    this.drawCircle(this.xPos, this.yPos, "primary");
   
    this.storeLastPosition(this.xPos, this.yPos);
  }
   
  private drawCircle(x, y, a) {
    var alpha;
    var scale;
    if (a == "primary") {
      // don't bother fading or scaling the trail "leader" :P
      alpha = 1;
      scale = 1;
    } else {
      // adjust the transparency and scale
      alpha = a / 2;
      scale = a;
    }
    this.context.beginPath();
    this.context.arc(x, y, scale * 10, 0, 2 * Math.PI, true);
    this.context.fillStyle = "rgba(121, 126, 135, " + alpha + ")";
    this.context.fill();
  }
  
  private storeLastPosition(xPos, yPos) {
    // push an item
    this.positions.push({
      x: xPos,
      y: yPos
    });
   
    //get rid of first item
    if (this.positions.length > this.motionTrailLength) {
      this.positions.shift();
    }
  }

  private clearCanvas(){
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.positions = [];
  }

}
