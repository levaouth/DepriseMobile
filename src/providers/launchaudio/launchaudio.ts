import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';


@Injectable()
export class LaunchaudioProvider {
  //soundPath: string = 'assets/sound/scene2/';
  constructor(public http: HttpClient,  public translateService: TranslateService) {
    //console.log('Hello LaunchaudioProvider Provider');
  }

  //a partir du chemin d'acces au dossier general de son, au nom precis de fichier
  //et d'un fichier ionic audio, lance le son de la voix
  launchAudio(sonSrc : string, audio : HTMLAudioElement, soundPath: string, lang: string){
  //si la piste audio precedent est terminee, on peut lancer le son
    if(audio.src == "" || audio.ended){
    //translateService permet de prendre la langue en cours
      let sourceAudio: string
      if(lang && lang != "" && lang == this.translateService.currentLang.toUpperCase()){
        sourceAudio = soundPath + lang + "_" + sonSrc;
      } else {
        sourceAudio = soundPath + sonSrc;
      }
      audio.src = sourceAudio;
      audio.load();
      audio.play();
    } else {
      //si la methode est appelee pendant qu'un fichier sonore est en cours de lecture
      //on attend la fin du fichier audio precedent
      setTimeout( ()=>{ this.launchAudio(sonSrc, audio, soundPath, lang); }, 150);
    }
  }

}
