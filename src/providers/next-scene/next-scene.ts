import { App } from "ionic-angular";
import { Injectable } from '@angular/core';


@Injectable()
export class NextSceneProvider {

  constructor(public app: App) {
  }

  nextScene(scene: any){
    let navs = this.app.getActiveNavs();
    if(scene != "HomePage")
      navs[0].push(scene);
    else
      navs[0].popToRoot();
  }
}
