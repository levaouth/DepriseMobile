import { Injectable } from '@angular/core';

import ShuffleText from '../../shuffle-text';

@Injectable()
export class ShuffleProvider {

  constructor() {}

  shuffle(textContainer, phraseToShow) {
    textContainer.nativeElement.innerHTML = phraseToShow;
    let shuffle = new ShuffleText(textContainer.nativeElement);
    shuffle.start();
  }

  //A utiliser lorsque textContainer est obtenu via "getElementById"
  shuffle2(textContainer, phraseToShow){
    textContainer.innerHTML = phraseToShow;
    let shuffle = new ShuffleText(textContainer);
    shuffle.start();
  }
}
