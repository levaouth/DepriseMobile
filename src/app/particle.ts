export interface Particle {
  size: number,
  position: { x: number, y: number },
  shift: { x: number, y: number },
  speed: number,
  colorIndex: any,
}
