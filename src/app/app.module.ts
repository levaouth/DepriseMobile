import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
// import { IonicSwipeAllModule } from 'ionic-swipe-all/dist/module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader} from '@ngx-translate/core';
import { TranslateHttpLoader} from '@ngx-translate/http-loader';
import { Keyboard } from '@ionic-native/keyboard';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Globalization } from '@ionic-native/globalization';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { Scene1PageModule } from '../pages/scene1/scene1.module';
import { Scene2PageModule } from '../pages/scene2/scene2.module';
import { Scene3PageModule } from '../pages/scene3/scene3.module';
import { Scene4PageModule } from '../pages/scene4/scene4.module';
import { Scene5PageModule } from '../pages/scene5/scene5.module';
import { Scene6PageModule } from '../pages/scene6/scene6.module';

import { ShuffleProvider } from '../providers/shuffle/shuffle';
import { NextSceneProvider } from '../providers/next-scene/next-scene';
import { SwipeFeedbackProvider } from '../providers/swipe-feedback/swipe-feedback';
import { LaunchaudioProvider } from '../providers/launchaudio/launchaudio';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    Scene1PageModule,
    Scene2PageModule,
    Scene3PageModule,
    Scene4PageModule,
    Scene5PageModule,
    Scene6PageModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    Globalization,
    InAppBrowser,
    StatusBar,
    SplashScreen,
    Keyboard,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ShuffleProvider,
    NextSceneProvider,
    SwipeFeedbackProvider,
    LaunchaudioProvider
  ]
})
export class AppModule {}
