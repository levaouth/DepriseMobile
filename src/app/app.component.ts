import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';
import { Keyboard } from '@ionic-native/keyboard';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Globalization } from '@ionic-native/globalization';



import { HomePage } from '../pages/home/home';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav:Nav;
  rootPage:any = HomePage;

  activeMenu: string = 'main'

  menus = {
    "main": {
      "name": "menu",
      "items": [
        {
          "name": "deprise",
          "forward": false,
          "action": {
            "type": "go",
            "param": "home"
          }
        },
        {
          "name": "presentation",
          "forward": false,
          "action": {
            "type": "go",
            "param": "PresentationPage"
          }
        },
        {
          "name": "credits",
          "forward": false,
          "action": {
            "type": "go",
            "param": "CreditsPage"
          }
        },
        /*{
          "name": "prix",
          "forward": false,
          "action": {
            "type": "go",
            "param": "PrixPage"
          }
        },*/
        {
          "name": "scenes",
          "forward": true,
          "action": {
            "type": "show",
            "param": "scenes"
          }
        },
        {
          "name": "langues",
          "forward": true,
          "action": {
            "type": "show",
            "param": "langues"
          }
        },
        {
          "name": "version_web",
          "forward": false,
          "action": {
            "type": "openUrl",
            "param": "http://www.deprise.fr"
          }
        },
        {
          "name": "confidentialite",
          "forward": false,
          "action": {
            "type": "go",
            "param": "ConfidentialitePage"
          }
        }
      ]
    },
    "langues": {
      "name": "langues",
      "items": [
        {
          "name": "francais",
          "forward": false,
          "action": {
            "type": "translate",
            "param": "fr"
          }
        },
        {
          "name": "spanish",
          "forward": false,
          "action": {
            "type": "translate",
            "param": "es"
          }
        },
        {
          "name": "english",
          "forward": false,
          "action": {
            "type": "translate",
            "param": "en"
          }
        },
        {
          "name": "italiano",
          "forward": false,
          "action": {
            "type": "translate",
            "param": "it"
          }
        },
        {
          "name": "portugues",
          "forward": false,
          "action": {
            "type": "translate",
            "param": "pt"
          }
        }
      ]
    },
    "scenes": {
      "name": "scenes",
      "items": [
        {
          "name": "scene1",
          "forward": false,
          "action": {
            "type": "go",
            "param": "Scene1Page"
          }
        },
        {
          "name": "scene2",
          "forward": false,
          "action": {
            "type": "go",
            "param": "Scene2Page"
          }
        },
        {
          "name": "scene3",
          "forward": false,
          "action": {
            "type": "go",
            "param": "Scene3Page"
          }
        },
        {
          "name": "scene4",
          "forward": false,
          "action": {
            "type": "go",
            "param": "Scene4Page"
          }
        },
        {
          "name": "scene5",
          "forward": false,
          "action": {
            "type": "go",
            "param": "Scene5Page"
          }
        },
        {
          "name": "scene6",
          "forward": false,
          "action": {
            "type": "go",
            "param": "Scene6Page"
          }
        }
      ]
    }
  }

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public iab: InAppBrowser, public globalization: Globalization,
              public menuCtrl: MenuController, public translateService: TranslateService, public keyboard: Keyboard) {
    platform.ready().then(() => {
      splashScreen.hide();

      // AJUSTEMENT DE LA LANGUE EN FONCTION DU TELEPHONE
      this.globalization.getPreferredLanguage()
        .then(res => {
          var lang = res.value.split("-")[0];
          translateService.setDefaultLang(lang);
          translateService.use(lang);
        })
        .catch(e => {
          translateService.setDefaultLang("en");
          translateService.use("en");
        });

      keyboard.disableScroll(true);   // empêche le scroll du contenu de l'écran vers le haut lorsque le clavier sort
    });
  }

  doThing(actionObject) {
    if (actionObject !== null) {
      if (actionObject.type === 'show') {
        this.show(actionObject.param);
      } else if (actionObject.type === 'translate') {
        this.translate(actionObject.param);
      } else if (actionObject.type === 'go') {
        this.go(actionObject.param);
      } else if (actionObject.type === 'openUrl') {
        this.openUrl(actionObject.param);
      }
    }
  }

  translate(code) {
    this.translateService.use(code);
    this.menuCtrl.close();
    this.activeMenu = 'main';
  }

  show(menu) {
    if (this.activeMenu) {
      this.activeMenu = menu;
    }
  }

  go(page) {
    if (page) {
      if (page === 'home') {
        this.nav.setRoot(HomePage);
      } else {
        this.nav.setPages([{page: HomePage}, {page: page}]);
      }
      this.menuCtrl.close();
      this.activeMenu = 'main';
    }
  }

  openUrl(url) {
    var target = "_blank";

    var options = "location=yes,hidden=yes";

    const inAppBrowserRef = this.iab.create(url, target, options);
    inAppBrowserRef.show();
  }
}
