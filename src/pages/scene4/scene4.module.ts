import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule} from '@ngx-translate/core';

import { Scene4Page } from './scene4';

@NgModule({
  declarations: [
    Scene4Page,
  ],
  imports: [
    IonicPageModule.forChild(Scene4Page),
    TranslateModule
  ],
})
export class Scene4PageModule {}
