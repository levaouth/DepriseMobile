import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable, Subscription } from 'rxjs/Rx';
import { TranslateService } from '@ngx-translate/core';
import { Gesture } from 'ionic-angular/gestures/gesture';

import { ShuffleProvider } from '../../providers/shuffle/shuffle';
import {SwipeFeedbackProvider} from '../../providers/swipe-feedback/swipe-feedback';
import { NextSceneProvider } from '../../providers/next-scene/next-scene';
import { LaunchaudioProvider } from '../../providers/launchaudio/launchaudio';
declare var Hammer: any;
@IonicPage()
@Component({
  selector: 'page-scene4',
  templateUrl: 'scene4.html',
})

// 4
// Si ce n'était qu'elle, je pourrais l'accepter.
// Mais mon fils dispose des mêmes armes.
// Il voudrait mon avis sur sa rédaction.
// Mais je ne parviens pas à me concentrer sur le texte.         ==> lancement du son maximilienEssai.mp3
// Étrange impression de ne pouvoir lire qu'entre les lignes…

export class Scene4Page {
  @ViewChild('textContainer', {read: ElementRef}) textContainer: ElementRef;
  @ViewChild('redaction', {read: ElementRef}) redaction: ElementRef;
  @ViewChild('cadre', {read: ElementRef}) cadre: ElementRef;
  @ViewChild('feedBackSwipeCanvas', {read: ElementRef}) feedBackSwipeCanvas: ElementRef;
  @ViewChild('animCanvas', {read: ElementRef}) animCanvas: ElementRef;

  translationKey: string = 'SCENE_4';
  //redactionFils="Je n'ai pas de héros. D'aussi loin qu'il m'en souvienne, et même après une longue réflexion, je n'ai jamais eu de héros. La figure du héros ne me séduit pas. Sans doute parce que je ne préfère pas une qualité à une autre, une valeur morale à une autre. Les héros, je les connais, je les reconnais, mais je n'éprouve ni adoration, ni idolâtrie pour eux...";
  redactionFils: string;


  phrases = [ // remarques du fils
     "Je ne t'aime pas.",
     "Tu ne me connais pas.",
     "Nous n'avons rien en commun.",
     "Je ne veux rien de toi.",
     "Tu n'es pas un modèle pour moi.",
     "Je veux voler de mes propres ailes.",
     "Bientot je partirai."
  ]

  sonsSrc =[
    "jenetaimepas.mp3",
    "tunemeconnaispas.mp3",
    "nousnavonsrienencommun.mp3",
    "jeneveuxriendetoi.mp3",
    "tunespasunmodelepourmoi.mp3",
    "jeveuxvolerdemespropresailes.mp3",
    "bientotjepartirai.mp3"
  ]
  cheminSons: string = 'assets/sound/scene4/';
  explosionAudio: HTMLAudioElement = new Audio();

  //permet de regler la classe des balises HTML (le gros 4, puis les textes en bas, etc)
  textContainerClass: string;
  phraseIndex: number;
  nbPhrases: number = 6;
  affichageRedaction: string="none";

  textContainerGesture: Gesture;

  lastSwipeTime: number = 0;  // cf swipe() : permet d'empêcher de prendre en compte plusieurs event swipe lors d'un seul mouvement.

  //son = 'assets/sound/scene4/maximilienEssay.mp3'; //penser a traduire ce path quand ce sera fonctionnel
  son = "redaction.mp3"
  //cheminSons : string = ''
  sonMoment: string; //l'attribut qui prendra la valeur de "essayMaximilien" au debut de l'affichage du texte,
  //puis des autres pistes son


  timer;
  sub: Subscription;


  //attributs utilises pour l'affichage de la redaction
  //MAX_X et MAX_Y definies dans ionViewDidLoad
  MAX_X; // rebord droit
  body;
  html;
  MAX_Y; // rebord bas
  MIN_X = 0; // rebord rebord gauche
  MIN_Y = 0; //rebord haut
  maxLength = 35; // longueur maximale d'une ligne (en caractères)
  MAX_DIST = 50; // taille du cercle de l'explosion
  nbLines = 10; // nombre de Lignes affichées ( doit être supérieur à 0 initialement, sera set plus tard correctement)

  table = []; // table contient l'ensemble des spans des lettres ( pour eviter les accès DOM)

  changementAutorise=true; // dis si l'on peut passer de j=4 à j=5
  audio:any // pointeur sur la source audio

  constructor(public navCtrl: NavController, public navParams: NavParams, public swipeFeedback : SwipeFeedbackProvider,
              public shuffleProvider: ShuffleProvider, public translateService: TranslateService, public launchaudioProvider: LaunchaudioProvider,  public nextSceneProvider: NextSceneProvider ) {}

  ionViewDidLoad() {
    // conditions initiales
    this.phraseIndex = 0;
    this.textContainerClass = 'num-chapitre';
    this.textContainerGesture = new Gesture(this.textContainer.nativeElement, {
      recognizers: [
        [Hammer.Swipe, {direction: Hammer.DIRECTION_ALL, velocity: 0.01}],
        [Hammer.Tap, {taps: 1}]
      ]
    });
    this.textContainerGesture.listen();
    // démarrage du timer
    this.timer = Observable.timer(0, 1000);
    this.sub = this.timer.subscribe((t)=>
      this.tickerFunc(t)
    );

    this.MAX_X = document.body.clientWidth; // rebord droit
    var body = document.body,
    html = document.documentElement;
    this.MAX_Y = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight ); // rebord bas
    //this.MAX_Y=0;
    //chacune des cases du tableau devient un tableau
    for (let i=0; i<150; i++){
       this.table[i] = [];
    }
  }

  ionViewDidLeave(){
    this.swipeFeedback.endFeedback();
  }

  //juste une fonction pour changer de scène sur la derniere phrase de celle-ci
  managePhraseEvents() {
    if (this.phraseIndex == 5) {  // Fin de scène
      this.nextSceneProvider.nextScene("Scene5Page");
    }
    else{
      this.shuffle();
    }
  }

  tickerFunc(tick) {
    if (tick == 3) {
      this.textContainerClass = 'text bottomFixed';
      this.phraseIndex++;
      this.swipeFeedback.initFeedback(this.feedBackSwipeCanvas.nativeElement, this.cadre);
      this.swipeFeedback.startFeedback();
      // ajout du listener sur le swipe
      this.textContainerGesture.on('swipe', () => {
        this.shuffle();
      });
      this.afficherRedaction();
    }
  }

  shuffle() {
    //this.changementAutorise permet de savoir si la redaction est affichee,
    //auquel cas ça vaut false et le swipe n'est pas permis tant que les explosions
    //de texte n'ont pas lieu
    if (this.phraseIndex < this.nbPhrases - 1 && this.changementAutorise) {
      let now = Date.now() / 1000;
      if (now - this.lastSwipeTime > 0.5) {
        this.lastSwipeTime = now;
        this.translateService.get(this.translationKey + '.phrases.ph' + ++this.phraseIndex).subscribe((translation: string) => {
          this.shuffleProvider.shuffle(this.textContainer, translation);
        });
      }
    }
    if(this.phraseIndex == 4){
      //ajouter la methode managePhraseEvents pour gérer les fins de swipe a la fin du codage de la scene
    /*  this.textContainerGesture.off('swipe', () => {
        this.managePhraseEvents();
      });*/
      //Deplacement de "Impossible de prononcer quelque chose de coherent" vers le bas
      //this.textContainerClass = "text bottomTransition";
      //on interdit le swiper avant que trois "explosions de texte" aient lieu
      this.swipeFeedback.stopFeedback();
      this.textContainerGesture.off('swipe', () => {
        this.managePhraseEvents();
      });
      this.changementAutorise=false;
      // this.sonMoment=this.cheminSons+this.translateService.currentLang.toUpperCase()+"_"+this.son;
      //this.launchaudioProvider.launchAudio(this.sonMoment,this.explosionAudio,this.cheminSons,this.translateService.currentLang.toUpperCase())

      //setTimeout(() => {  }, 1000);
      // setTimeout(() => { this.initiateCanvas() }, 10000);
      this.initiateCanvas();
    }
    // A l'affichage de "Etrange impression...", transformer la phrase en un lien vers la scene 5
    if (this.phraseIndex==5){
      this.swipeFeedback.stopFeedback();
      this.textContainerGesture.off('swipe', () => {
        this.managePhraseEvents();
      });
      this.textContainerGesture.on('tap', () => {
        //lancement de la phrase suivante
        this.managePhraseEvents();
      });
      //chgt de classe
      this.textContainerClass = "textFinScene bottomTransition";
    }
  }

  afficherRedaction(){
    //traduction de la redaction dans la bonne langue
    this.translateService.get(this.translationKey + '.redaction.texte').subscribe((translation: string) => {
      //this.shuffleProvider.shuffle(this.textContainer, translation);
      this.redactionFils=translation;
    });

    //traduction de chacune des phrases "derriere le texte" dans la bonne langue
    for (var i=0; i<this.phrases.length;i++){
      this.translateService.get(this.translationKey + '.redaction.explosion' + i).subscribe((translation: string) => {
        //this.shuffleProvider.shuffle(this.textContainer, translation);
        this.phrases[i]=translation;
      });
    }
    this.affichageRedaction="inline";
    //on evite que le champ de texte disparaisse apres le deplacement en changeant sa classe CSS
    //this.textContainerClass="text bottomFixed";

    //affichage de la rédaction
    // setTimeout( ()=> { this.wordWrapper('container', this.redactionFils); }, 1000);
    this.wordWrapper('container', this.redactionFils);
  }

  wordWrapper( id, str){
  // Divise la rédaction en lignes de taille à peu près équivalentes et génère les spans qui y correspondent
    let elt = document.getElementById( String(id) );
    //chaque mot de la redaction devient stocke dans un tableau de mots
    let words = str.split(" ");
    let i;
    let y=0;
    let curLine;
    let lineLength = 0;
    let myBreak;
    let mySpan;

    //ajout de chacun des mots de la redaction sous la forme d'un tableau
    for ( i = 0; i< words.length; i++){ // on ajoute les mots un par un dans la ligne jusqu'à avoir la bonne taille
      //si la ligne n'a pas deja ete entamee ou si la ligne actuelle est trop longue,
      //on cree une nouvelle ligne
      if  ( lineLength==0 || lineLength > this.maxLength-10){ // 20 est arbitraire (nb de caracteres par ligne)

        //creation des elements correspondants a la nouvelle ligne
        curLine = document.createElement('span');
        myBreak = document.createElement('br');
        mySpan = document.createElement('span');

        //ajout de la classe "line" donnant la taille de police de la ligne
        curLine.setAttribute('class', 'line');
        curLine.appendChild( mySpan );

        //ajout de la ligne sur le conteneur
        elt.appendChild( curLine );

        //ajout du saut de ligne a la fin de ce conteneur
        elt.appendChild(myBreak);

        //on n'affiche pas cette nouvelle ligne tant que les lettres n'ont pas fini de se deplacer
        mySpan.style.display = "none";

        //l'id du span est celui de la ligne
        mySpan.id = y;
        y++;
      }

      //ajout d'un mot sur une ligne du texte
      mySpan.innerHTML += words[i] + ' ';

      //lineLength prend la longueur (nb de caracteres ou de mot?)
      //du contenu du span ajoute
      lineLength = mySpan.innerHTML.length;
    }

    //a la fin de la boucle ci-dessus, "y" a enregistre le nombre de lignes
    this.nbLines = y;

    // lance l'apparition
    this.displayText();
  }


  displayText( lineNumber=0 ){
    // affiche le texte avec l'animation
    let lineNumberStr = String( lineNumber );

    //selection de la ligne grace a son identifiant qui est le chiffre
    //(rappel: dans la boucle de wordWrapper, mySpan.id=y)
    let line:any =document.getElementById( lineNumberStr );

    // lance l'affichage d'une ligne si celle-ci existe (géré par popLetter)
    if ( line ){
      this.letterWrapper( line.parentElement, line.innerText, line.innerText.length, lineNumber, true);
      this.popLetter( 0, lineNumber );
    }else{
      this.showReady = true;
    }
  }


  letterWrapper(elt, str, maxlength, y, hideLetter=false ){
  // on divise une ligne en lettres, même fonctionnement que wordWrapper
  // hideletter est utile pour le premier affichage seulement
    let i;
    let mySpan;

    //chacune des lettres est inseree dans le tableau "table"
    for (let x = 0; x < str.length; x++){
       mySpan = document.createElement('span');
       mySpan.innerHTML = str[x];
       this.table[x][y] = mySpan;
       elt.appendChild( mySpan );
    }

    //affichage des lettres en position absolues
    for ( i = str.length; i>=0; i-- ){
    let letter = this.table[i][y];
      if ( letter && letter.innerHTML){
          letter.style.position = "fixed";
          let left = letter.getBoundingClientRect().left;
          let top = letter.getBoundingClientRect().top;
          letter.style.top = top+"px";
          letter.style.left = left+"px";
          if( hideLetter == true ){ letter.style.display = "none"; }
      }
    }
  }

 popLetter( x :number, y :number  ){
   // affiche une lettre avec une zolie transition
  // console.log("MEME LA PROD ELLE SAIT PASSER PAR POPLETTER")
   //if ( !this.skipAnimation ){
     let elt = this.table[x][y];
     if (y > this.nbLines){
     }
     else if( elt ){ //si elt a bien permis de prendre un element de table, alors
       //on prend ses coordonnees sur la page dans left et top
       let left = elt.style.left;
       let top = elt.style.top;

       //coordonnes du point au debut de l'animation (determinees aleatoirement)
       let left0 :number;
       let top0 :number;
       let rand = Math.floor( Math.random() * 3);
       //choix du bord ou on veut faire apparaitre la lettre avant qu'elle rejoigne
       //sa bonne position dans le texte (sans le bord haut)
       switch (rand){
         case 0: left0 = this.MIN_X - 10;
                 top0 = Math.random() * (this.MIN_Y+this.MAX_Y) - this.MIN_Y;
                 break;
         case 1: left0 = this.MAX_X;
                 top0 = Math.random() * (this.MIN_Y+this.MAX_Y) - this.MIN_Y;
                 break;
         case 2: top0 = this.MAX_Y;
                 left0 = Math.random() * (this.MIN_X+this.MAX_X) - this.MIN_X;
                 break;
       }

       let deg = 180;


       //NEW
       let letterMove : letterMove = {
        endPositionY : Number(top.replace("px","")),
        endPositionX : Number(left.replace("px","")),
        startPositionX : left0,
        startPositionY : top0,
        startRotation : deg,
        text : elt.innerHTML,
        currentPositionX : left0,
        currentPositionY : top0,
        currentRotation : deg,
        time : 0,
        speed : 0,
        angle : 0,
        partOfExplosion : false
      };
      this.lettersAnim[this.indexLetterAnim] = letterMove;
      this.indexLetterAnim++;
      //END NEW

      // affichage de la lettre suivante
      this.popLetter( x+1, y );
       } else {
        // on lance l'affichage de la ligne suivante
        this.displayText(y+1);
       }
  }


/*
*************************************
*                                   *
*     NEW code avec canvas          *
*                                   *
*************************************
*/
  indexEnd : number = 0;
  cadreGesture: Gesture;
  showReady : boolean = false;
  lettersAnim : letterMove[] = [];
  indexLetterAnim : number = 0;
  SCREEN_WIDTH: number = window.innerWidth;
  SCREEN_HEIGHT: number = window.innerHeight;
  canvasCenter : {
    x : number,
    y : number
  } = {x:0,y:0};
  refreshTimer;
  refreshTimerSub : Subscription;

  fps : number = 60;
  durationAnimOneLetter : number = 1;

  centerExplosionX : number;
  centerExplosionY : number;
  textExplosion : string;
  speedValue : number = 2500;
  //augmenter speedValue augmente la taille de l'explosion de texte
  timeExplode : number = 0;
  //durée de l'explosion lorsqu'on tape sur le texte (pas du retour)
  durationExplode : number = 5*this.fps;
  //temps en sec soustrait à durationExplode
  substractDurationFinishBeforeEnd : number = 1.5;
  lockCreateExplode : boolean = false;

  timeComeBack : number = 0;
  
  //si on souhaite ralentir l'animation d'explosion de texte,
  //augmenter cette valeur (qui était à 2 avant que M. Bouchardon ne demande d'accelerer)
  durationComeBack : number = 1;
  durationTextClear : number = 0.95;

  MAX_CANVAS_X : number;
  MAX_CANVAS_Y : number;
  MIN_CANVAS_X : number;
  MIN_CANVAS_Y : number;


  initiateCanvas(){
    if(!this.showReady){
      setTimeout(() => {this.initiateCanvas()}, 500);
      return;
    }
    this.sonMoment=this.cheminSons+this.translateService.currentLang.toUpperCase()+"_"+this.son;
    this.SCREEN_HEIGHT = window.innerHeight;
    this.SCREEN_WIDTH = window.innerWidth;

    this.refreshTimer = Observable.timer(0, 1000/this.fps);
    this.refreshTimerSub = this.refreshTimer.subscribe(() => {
        this.updateCanvasLayoutShowText();
      }
    );

    this.initCanvas();

    let canvasPosition = this.animCanvas.nativeElement.getBoundingClientRect();
    this.canvasCenter.x  = canvasPosition.width/2;
    this.canvasCenter.y  = canvasPosition.height/2;

    let ctx = this.animCanvas.nativeElement.getContext('2d');
    // ctx.textAlign = 'center';
    ctx.font = 'italic  16px Arial';
    ctx.fillStyle = 'rgba(255, 255, 255, 1)';
    this.indexLetterAnim = 0;
    ctx.save();
  }

  pixelRatio: number;
  initCanvas() {
    let ctx = this.animCanvas.nativeElement.getContext("2d");

    let dpr = window.devicePixelRatio || 1;
    let bsr = ctx.webkitBackingStorePixelRatio ||
          ctx.mozBackingStorePixelRatio ||
          ctx.msBackingStorePixelRatio ||
          ctx.oBackingStorePixelRatio ||
          ctx.backingStorePixelRatio || 1;
    this.pixelRatio = dpr / bsr;

    this.animCanvas.nativeElement.width = this.SCREEN_WIDTH * this.pixelRatio;
    this.animCanvas.nativeElement.height = this.SCREEN_HEIGHT * this.pixelRatio;
    this.animCanvas.nativeElement.style.width = this.SCREEN_WIDTH + "px";
    this.animCanvas.nativeElement.style.height = this.SCREEN_HEIGHT + "px";
    ctx.setTransform(this.pixelRatio, 0, 0, this.pixelRatio, 0, 0);
  }

  updateCanvasLayoutShowText(){
    let ctx : CanvasRenderingContext2D = this.animCanvas.nativeElement.getContext('2d');
    let letter : letterMove = this.lettersAnim[this.indexLetterAnim];
    if(letter == null){
      return;
    }
    ctx.clearRect(0, 0, this.animCanvas.nativeElement.width, this.animCanvas.nativeElement.height);

    // ctx.restore();
    // ctx.rotate(letter.currentRotation * Math.PI / 180);
    // ctx.fillStyle = 'rgba(0, 0, 0, 1)';
    // ctx.fillText(letter.text, letter.currentPositionX, letter.currentPositionY);

    // if( this.time == this.durationAnimOneLetter*this.fps){

    // }
    // letter.currentRotation = letter.currentRotation - (letter.startRotation) / this.fps*0.5;
    // ctx.restore();
    ctx.fillStyle = 'rgba(255, 255 , 255, 1)';
    // ctx.rotate(letter.currentRotation * Math.PI / 180);
    // ctx.fillText(letter.text, letter.currentPositionX, letter.currentPositionY);
    for(let i = 0; i<= this.indexLetterAnim; i++){
      let letterTMP : letterMove = this.lettersAnim[i];
      letterTMP.currentPositionX = this.easeOutCubic(letterTMP.time, letterTMP.startPositionX, letterTMP.endPositionX, this.durationAnimOneLetter * this.fps);
      letterTMP.currentPositionY = this.easeOutCubic(letterTMP.time, letterTMP.startPositionY, letterTMP.endPositionY, this.durationAnimOneLetter * this.fps);
      ctx.fillText(letterTMP.text, letterTMP.currentPositionX, letterTMP.currentPositionY);
      if(! (letterTMP.time == this.durationAnimOneLetter*this.fps)){
        letterTMP.time ++;
      }
    }
    if( letter.time >= 0.03*this.durationAnimOneLetter*this.fps && this.lettersAnim.length - 1 > this.indexLetterAnim){
      this.indexLetterAnim++;
    }
    if(this.lettersAnim.length - 1 == this.indexLetterAnim && letter.time == this.durationAnimOneLetter*this.fps){
      this.refreshTimerSub.unsubscribe();
      this.initExplode();
    }
  }

  initExplode(){
    this.cadreGesture = new Gesture(this.cadre.nativeElement,  {
      recognizers: [
        [Hammer.Tap, {taps: 1}],
      ]
    });
    this.cadreGesture.listen();

    this.cadreGesture.on('tap', (event) => {
      this.createExplosion(event);
    });

    for (var i=0; i<this.phrases.length;i++){
      this.translateService.get(this.translationKey + '.redaction.explosion' + i).subscribe((translation: string) => {
        this.phrases[i]=translation;
      });
    }
    this.MAX_CANVAS_X = this.SCREEN_WIDTH;
    this.MAX_CANVAS_Y = this.SCREEN_HEIGHT;
    this.MIN_CANVAS_X = 0;
    this.MIN_CANVAS_Y = 0;
  }

//  t : timer, b : startValue, c : EndValue, d : duration
  easeOutCubic(t, b, c, d) {
    t /= d;
    t--;
    c = c - b;
    return c*(t*t*t + 1) + b;
  }

  easeInCubic(t, b, c, d) {
    t /= d;
    c = c - b;
    return c*t*t*t + b;
  };

  createExplosion(event){
    if(this.lockCreateExplode)
      return;
    
    let ExplodeAutorise : boolean = false;
    let n = Math.floor( Math.random()* this.phrases.length);
    this.textExplosion = this.phrases[n];
    let MAX_DIST = this.textExplosion.length * 9 + 150;
    this.centerExplosionX = Math.floor(event.center.x);
    this.centerExplosionY = Math.floor(event.center.y);
    for(let letter of this.lettersAnim){
      let distX = letter.currentPositionX - this.centerExplosionX;
      let distY = letter.currentPositionY - this.centerExplosionY;
      let dist = Math.sqrt( Math.pow(distX, 2) + Math.pow(distY, 2) ); // distance entre l'objet et l'épicentre
      if( Math.abs(distX) < MAX_DIST && Math.abs(distY) < 75 ){
        letter.partOfExplosion = true;
        let cos = distX / dist;
        let sin = distY / dist;
        let theta = Math.acos( cos ) % (Math.PI * 2);
        if ( dist == 0 ){
        	theta = Math.PI/2;
        	cos = 0;
        	sin = 1;
        }
        if (sin > 0){
        	theta = -theta;
        }else if ( (sin === 0) ){
          theta+= 0.1 * (Math.random() - 0.5 ) ;
        }

        theta+= 0.2 * (Math.random() - 0.5 ) ;
        letter.angle = theta;
        letter.speed = this.speedValue/Math.pow(dist,2);
        if(letter.speed > 4)
          ExplodeAutorise = true;
      }
    }
    if(!ExplodeAutorise){
      return;
    }
    if(this.indexEnd == 3){
      this.changementAutorise = true;
      this.swipeFeedback.startFeedback();
      this.textContainerGesture.on('swipe', () => {
        this.shuffle();
      });
      this.shuffle();
    }
    this.indexEnd++;
    this.lockCreateExplode = true;
    this.sonMoment = this.sonsSrc[n];
    this.launchaudioProvider.launchAudio(this.sonMoment,this.explosionAudio,this.cheminSons,this.translateService.currentLang.toUpperCase())
    //this.audio.play();
    this.refreshTimerSub = this.refreshTimer.subscribe(() => {
        this.updateCanvasLayoutExplode();
      }
    );
  }

  updateCanvasLayoutExplode(){
    let ctx : CanvasRenderingContext2D = this.animCanvas.nativeElement.getContext('2d');
    ctx.clearRect(0, 0, this.animCanvas.nativeElement.width, this.animCanvas.nativeElement.height);
    ctx.fillStyle = 'rgba(255,255,255,0.6)';
    ctx.textAlign = 'center';
    ctx.fillText(this.textExplosion,this.centerExplosionX,this.centerExplosionY);
    ctx.fillStyle= 'rgba(255,255,255,1)';
    ctx.textAlign = 'start';
    for(let letter of this.lettersAnim){
      if(letter.partOfExplosion){

        if(letter.currentPositionX > this.MAX_CANVAS_X){
          if(letter.angle > 0)
            letter.angle = Math.PI - letter.angle % (Math.PI*2);
          else
            letter.angle = Math.PI*1.5 + letter.angle % (Math.PI*2);
        }else if (letter.currentPositionX < this.MIN_CANVAS_X){
          if(letter.angle > Math.PI)
            letter.angle = letter.angle + (Math.PI/2) % (2*Math.PI);
          else
            letter.angle = Math.PI - letter.angle % (2*Math.PI);
        }else if(letter.currentPositionY > this.MAX_CANVAS_Y || letter.currentPositionY < this.MIN_CANVAS_Y){
          letter.angle = -letter.angle % (Math.PI*2);
        }

        letter.currentPositionY += - (letter.speed*(this.durationExplode - this.easeOutCubic(this.timeExplode,0,this.durationExplode,this.durationExplode))/this.durationExplode) * Math.sin(letter.angle) ;
        letter.currentPositionX +=  (letter.speed*(this.durationExplode - this.easeOutCubic(this.timeExplode,0,this.durationExplode,this.durationExplode))/this.durationExplode) * Math.cos(letter.angle);
      }
      ctx.fillText(letter.text, letter.currentPositionX, letter.currentPositionY);
    }
    if(this.timeExplode == (this.durationExplode - this.substractDurationFinishBeforeEnd * this.fps)){
      for(let letter of this.lettersAnim){
        letter.startPositionX = letter.currentPositionX;
        letter.startPositionY = letter.currentPositionY;
      }
      this.refreshTimerSub.unsubscribe();
      this.refreshTimerSub = this.refreshTimer.subscribe(() => {
        this.updateCanvasLayoutComeBackToHomeLittleLetters();
      }
    );
    }
    this.timeExplode ++;
  }

  updateCanvasLayoutComeBackToHomeLittleLetters(){
    let ctx : CanvasRenderingContext2D = this.animCanvas.nativeElement.getContext('2d');
    ctx.clearRect(0, 0, this.animCanvas.nativeElement.width, this.animCanvas.nativeElement.height);
    if(this.timeComeBack <= this.durationTextClear*this.fps){
      ctx.fillStyle = 'rgba(255,255,255,0.6)';
      ctx.textAlign = 'center';
      ctx.fillText(this.textExplosion,this.centerExplosionX,this.centerExplosionY);
      ctx.fillStyle= 'rgba(255,255,255,1)';
      ctx.textAlign = 'start';
    }
    for(let letter of this.lettersAnim){
      if(letter.partOfExplosion){
        letter.currentPositionX = this.easeInCubic(this.timeComeBack, letter.startPositionX, letter.endPositionX, this.durationComeBack * this.fps);
        letter.currentPositionY = this.easeInCubic(this.timeComeBack, letter.startPositionY, letter.endPositionY, this.durationComeBack * this.fps);
      }
      ctx.fillText(letter.text, letter.currentPositionX, letter.currentPositionY);
    }
    if(this.timeComeBack == this.durationComeBack*this.fps){
      this.refreshTimerSub.unsubscribe();
      for(let letter of this.lettersAnim){
        letter.partOfExplosion = false;
      }
      this.timeComeBack = 0;
      this.timeExplode = 0;
      this.lockCreateExplode = false;
    }
    this.timeComeBack++;
  }

}


interface letterMove{
  startPositionX : number,
  startPositionY : number,
  endPositionX : number,
  endPositionY : number,
  startRotation : number,
  text : string,
  currentPositionX : number,
  currentPositionY : number,
  currentRotation : number,
  time : number,
  speed : number,
  angle : number,
  partOfExplosion : boolean
}

/*
Pour les fonctions d'easing :
http://www.gizma.com/easing
*/
