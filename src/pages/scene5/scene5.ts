import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { Observable, Subscription } from 'rxjs/Rx';
import { TranslateService } from '@ngx-translate/core';
import { Gesture } from 'ionic-angular/gestures/gesture';
import { CameraPreview, CameraPreviewPictureOptions, CameraPreviewOptions } from '@ionic-native/camera-preview';
import { LaunchaudioProvider } from '../../providers/launchaudio/launchaudio';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { ShuffleProvider } from '../../providers/shuffle/shuffle';
import {SwipeFeedbackProvider} from '../../providers/swipe-feedback/swipe-feedback';
import { NextSceneProvider } from '../../providers/next-scene/next-scene';
import { GyroNorm } from '../../gyronorm.complete';

declare var Hammer: any;

@IonicPage()
@Component({
  selector: 'page-scene5',
  templateUrl: 'scene5.html',
  providers: [
    CameraPreview,
    AndroidPermissions,
    ScreenOrientation
  ]
})

// 5
// Suis-je si peu présent ?
// Si modelable ?
// Ma propre image semble me fuir.
// Elle m'échappe.       => deplacement vers le bas de l'image
// Je me sens manipulé.

export class Scene5Page {
  @ViewChild('textContainer', {read: ElementRef}) textContainer: ElementRef;
  @ViewChild('imageCanvas', {read: ElementRef}) imageCanvas: ElementRef;
  @ViewChild('cadre', {read: ElementRef}) cadre: ElementRef;
  @ViewChild('feedBackSwipeCanvas', {read: ElementRef}) feedBackSwipeCanvas: ElementRef;

  translationKey: string = 'SCENE_5';

  textContainerClass: string
  phraseIndex: number;
  nbPhrases: number = 6;

  textContainerGesture: Gesture;
  ImageGesture : Gesture;
  lastSwipeTime: number;  // cf swipe() : permet d'empêcher de prendre en compte plusieurs event swipe lors d'un seul mouvement.

  timer;
  sub: Subscription;

  tabClearTimeout: number[] = [];

  sonScr: string = 'Rubric.mp3';
  soundPath: string = 'assets/sound/scene5/';
  audioMusic : HTMLAudioElement = new Audio();

  saveCanvas : HTMLCanvasElement[] = [];

  cameraPreviewOpts: CameraPreviewOptions = {
    x: 0,
    y: 0,
    width: window.screen.width,
    height: window.screen.height,
    camera: 'front',
    tapPhoto: true,
    previewDrag: true,
    toBack: true,
    alpha: 1
  };

  pictureOpts: CameraPreviewPictureOptions = {
    width: 1280,
    height: 1280,
    quality: 85
  }

  img : HTMLImageElement;

  gyroNorm : any;
  initialOrientation = {
    alpha : 0,
    beta : 0,
    gamma : 0
  };
  gyroscopePositionX : number = 0;
  gyroscopePositionY : number = 0;

  valuesTwirl = {
    radius : 75, // rayon du twirl
    angle : 15, // angle de rotation (+/- déformé rapidement en quittant le centre)
    centerX : 0.5, // centre x en % de l'image
    centerY : 0.5 // centre y en % de l'image
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public swipeFeedback : SwipeFeedbackProvider,
              public shuffleProvider: ShuffleProvider, public translateService: TranslateService,
              public cameraPreview: CameraPreview,  public launchaudioProvider: LaunchaudioProvider,
              public nextSceneProvider : NextSceneProvider, public platform: Platform,
              public androidPermissions: AndroidPermissions, public screenOrientation: ScreenOrientation) {}

  ionViewDidLoad() {
    // conditions initiales
    this.phraseIndex = 0;
    this.textContainerClass = 'num-chapitre';
    this.lastSwipeTime = 0;
    this.textContainerGesture = new Gesture(this.textContainer.nativeElement, {
      recognizers: [
        [Hammer.Swipe, {direction: Hammer.DIRECTION_ALL, velocity: 0.01}],
        [Hammer.Tap, {taps: 1}]
      ]
    });
    this.textContainerGesture.listen();
    // démarrage du timer
    this.timer = Observable.timer(0, 1000);
    this.sub = this.timer.subscribe((t)=>
      this.tickerFunc(t)
    );
    this.gyroNorm = new GyroNorm();
    var args = {
      frequency:10000,					// ( How often the object sends the values - milliseconds )
    };
    this.gyroNorm.init(args);

    // verouillage en mode portrait
    if (this.platform.is('cordova')) {
      this.platform.ready().then(() => {
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      });
    }
  }

  ionViewDidLeave(){
    this.swipeFeedback.endFeedback();
    this.sub.unsubscribe();
    // this.cameraPreview.stopCamera();
    this.audioMusic.pause();
    this.audioMusic.src = "";
    this.textContainerGesture.destroy();
    if(this.ImageGesture && this.ImageGesture != undefined)
      this.ImageGesture.destroy();
    if(this.gyroNorm && this.gyroNorm != undefined){
      this.gyroNorm.end();
    }
    this.clearAllTimeout();
  }

  clearAllTimeout(){
    for(let timeout of this.tabClearTimeout){
      clearTimeout(timeout);
    }
  }

  tickerFunc(tick) {
    if (tick == 3) {
      this.textContainerClass = 'text';
      this.phraseIndex++;
      this.swipeFeedback.initFeedback(this.feedBackSwipeCanvas.nativeElement, this.cadre);
      this.swipeFeedback.startFeedback();
      // ajout du listener sur le swipe
      this.textContainerGesture.on('swipe', () => {
        this.shuffle();
      });
      this.imageCanvas.nativeElement.width =  this.imageCanvas.nativeElement.offsetWidth;
      this.imageCanvas.nativeElement.height =  this.imageCanvas.nativeElement.offsetHeight;
      this.sub.unsubscribe();
      if (this.platform.is('android')) {
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA]);
      }
      this.cameraPreview.startCamera(this.cameraPreviewOpts);
    }
  }

  shuffle() {
    if (this.phraseIndex < this.nbPhrases - 1) {
      let now = Date.now() / 1000;
      if (now - this.lastSwipeTime > 0.5) {
        this.lastSwipeTime = now;
        this.translateService.get(this.translationKey + '.phrases.ph' + ++this.phraseIndex).subscribe((translation: string) => {
          this.shuffleProvider.shuffle(this.textContainer, translation);
        });
      }
    }
    if(this.phraseIndex == 4) {
      this.swipeFeedback.endFeedback();
      this.textContainerGesture.off('swipe', () => {
        this.shuffle();
      });
      this.textContainerClass = "text bottomTransition";
      this.launchCamera();
    }
    if(this.phraseIndex == 5) {
      this.textContainerGesture.on('tap', () => {
        this.nextSceneProvider.nextScene("Scene6Page");
      });
      this.textContainerClass = "textFinScene bottomTransition";
    }
  }

  launchCamera() {
    this.cameraPreview.takePicture(this.pictureOpts).then((imageData) => {
      this.initImage(imageData);
    }, (err) => {
      this.initImage();
    });
  }

  initImage(imageData = null) {
    this.img = new Image();
    this.img.addEventListener("load", () => {
      let canvas = this.imageCanvas.nativeElement;
      let ctx = canvas.getContext('2d');
      ctx.drawImage(this.img, 0, 0, canvas.width, canvas.height);
      this.saveCanvas[0] = document.createElement('canvas');
      this.saveCanvas[0].width = canvas.width;
      this.saveCanvas[0].height = canvas.height;
      this.saveCanvas[0].getContext('2d').drawImage(this.img, 0, 0, canvas.width, canvas.height);
    });

    if (imageData !== null) {
      this.img.src = 'data:image/jpeg;base64,' + imageData;
    } else {
      this.img.src = 'assets/imgs/CharacterDraw.png';
    }
    this.cameraPreview.stopCamera();
    this.initEffects();
  }

  initEffects() {
    this.ImageGesture = new Gesture(this.cadre.nativeElement,  {
      recognizers: [
        [Hammer.Pan, {direction: Hammer.DIRECTION_ALL}],
      ]
    });
    this.ImageGesture.listen();
    this.ImageGesture.on('pan', (event) => {
      this.applyFilterTwirl(event)
    });
    let liquifyTimer = Observable.timer(0, 1000/20);
      liquifyTimer.subscribe(() =>
        this.loopLiquify()
    );
    this.launchaudioProvider.launchAudio(this.sonScr,this.audioMusic,this.soundPath,null);
    this.tabClearTimeout[this.tabClearTimeout.length] = setTimeout(()=>this.shuffle(),30000);
    this.initLiquify();
  }

  applyFilterTwirl(evt){
    let centerX = Math.floor(evt.center.x);
    let centerY = Math.floor(evt.center.y);
    let width = this.valuesTwirl.radius*2;
    let leftPosition = centerX - width/2;
    let topPosition = centerY - width/2;
    var data = this.imageCanvas.nativeElement.getContext('2d').getImageData(leftPosition,topPosition, width, width);
    this.filterTwirl(data, this.valuesTwirl);
    this.imageCanvas.nativeElement.getContext('2d').putImageData(data,leftPosition,topPosition);
  }

  filterTwirl (input, values){
    var width = input.width, height = input.height;
    var inputData = input.data;

    var angle =  values.angle;
    var centerX = values.centerX;
    var centerY =  values.centerY;
    var radius = values.radius;
    var radius2 = radius*radius;
    angle = angle/180 * Math.PI;
    var iCenterX = width * centerX; var iCenterY = height * centerY;
    var transInverse = function(x,y,out){
      var dx = x-iCenterX;
      var dy = y-iCenterY;
      var distance = dx*dx + dy*dy;
      if(distance > radius2){
        out[0] = x;
        out[1] = y;
      } else {
        distance = Math.sqrt(distance);
        var a = Math.atan2(dy, dx) + angle * (radius-distance) / radius;
        out[0] = iCenterX + distance*Math.cos(a);
        out[1] = iCenterY + distance*Math.sin(a);
      }
    };
    this.transformFilter(inputData,transInverse,width,height);
  }

  transformFilter(inputData, transformInverse, width, height){
  	var out = [];
  	var outputData = [];

  	for(var y = 0; y < height; y++){
 		  for (var x = 0; x < width; x++){
 		  	var pixel = (y*width + x)*4;
		  	transformInverse.apply(this,[x,y,out]);
		    var srcX = Math.floor(out[0]);
		    var srcY = Math.floor(out[1]);
		    var xWeight = out[0]-srcX;
		    var yWeight = out[1]-srcY;
		    var nw,ne,sw,se;
		    if(srcX >= 0 && srcX < width-1 && srcY >= 0 && srcY < height-1){
		    	var i = (width*srcY + srcX)*4;
		    	nw = [inputData[i],inputData[i+1],inputData[i+2],inputData[i+3]];
		    	ne = [inputData[i+4],inputData[i+5],inputData[i+6],inputData[i+7]];
		    	sw = [inputData[i+width*4],inputData[i+width*4+1],inputData[i+width*4+2],inputData[i+width*4+3]];
		    	se = [inputData[i+(width + 1)*4],inputData[i+(width + 1)*4+1],inputData[i+(width + 1)*4+2],inputData[i+(width + 1)*4+3]];
		    } else {
	    		nw = this.getPixel( inputData, srcX, srcY, width, height );
		  	  ne = this.getPixel( inputData, srcX+1, srcY, width, height );
		  	  sw = this.getPixel( inputData, srcX, srcY+1, width, height );
		  	  se = this.getPixel( inputData, srcX+1, srcY+1, width, height );
		    }
		    var rgba = this.bilinearInterpolate(xWeight,yWeight,nw,ne,sw,se);
		  	outputData[pixel] = rgba[0];
		  	outputData[pixel + 1] = rgba[1];
		  	outputData[pixel + 2] = rgba[2];
		  	outputData[pixel + 3] = rgba[3];
 		  }
 	  }
 	  for(var k = 0; k < outputData.length; k++){
  		inputData[k] = outputData[k];
  	}
  }

  getPixel (pixels,x,y,width,height){
		var pix = (y*width + x)*4;
		if (x < 0 || x >= width || y < 0 || y >= height) {
			return [pixels[((this.clampPixel(y, 0, height-1) * width) + this.clampPixel(x, 0, width-1))*4],
			pixels[((this.clampPixel(y, 0, height-1) * width) + this.clampPixel(x, 0, width-1))*4 + 1],
			pixels[((this.clampPixel(y, 0, height-1) * width) + this.clampPixel(x, 0, width-1))*4 + 2],
			pixels[((this.clampPixel(y, 0, height-1) * width) + this.clampPixel(x, 0, width-1))*4 + 3]];
		}
		return [pixels[pix],pixels[pix+1],pixels[pix+2],pixels[pix+3]];
  }

  clampPixel(x,a,b){
		return (x < a) ? a : (x > b) ? b : x;
  }

   bilinearInterpolate (x,y,nw,ne,sw,se){
    var m0, m1;
    var r0 = nw[0]; var g0 = nw[1]; var b0 = nw[2]; var a0 = nw[3];
    var r1 = ne[0]; var g1 = ne[1]; var b1 = ne[2]; var a1 = ne[3];
    var r2 = sw[0]; var g2 = sw[1]; var b2 = sw[2]; var a2 = sw[3];
    var r3 = se[0]; var g3 = se[1]; var b3 = se[2]; var a3 = se[3];
    var cx = 1.0 - x; var cy = 1.0 - y;

    m0 = cx * a0 + x * a1;
    m1 = cx * a2 + x * a3;
    var a = cy * m0 + y * m1;

    m0 = cx * r0 + x * r1;
    m1 = cx * r2 + x * r3;
    var r = cy * m0 + y * m1;

    m0 = cx * g0 + x * g1;
    m1 = cx * g2 + x * g3;
    var g = cy * m0 + y * m1;

    m0 = cx * b0 + x * b1;
    m1 = cx * b2 + x * b3;
    var b =cy * m0 + y * m1;
    return [r,g,b,a];
  }

  positionLiquids : positionLiquid[] = [];
  oldPositionLiquids : positionLiquid[] = [];

  initLiquify(){
    for(var i=0;i<10;i++){
      this.positionLiquids[i] = {positionX :  Math.floor(Math.random() * Math.floor(this.imageCanvas.nativeElement.width)),
                                positionY :  Math.floor(Math.random() * Math.floor(this.imageCanvas.nativeElement.height))}
      this.oldPositionLiquids[i] = {positionX : this.positionLiquids[i].positionX, positionY : this.positionLiquids[i].positionY};
    }

    this.platform.ready().then ( () => {
      if (this.platform.is('cordova')){
        var args = {
          frequency:50,					// ( How often the object sends the values - milliseconds )
        };
        this.gyroNorm.init(args).then(()=>{
          this.gyroNorm.start((data)=>{
            // console.log('Alpha :' + data.do.alpha);
            // console.log('Beta : ' + data.do.beta);
            // console.log(data.do.gamma);
            if(this.initialOrientation.beta == 0 && this.initialOrientation.alpha == 0){
              this.initialOrientation.beta = data.do.beta;
              this.initialOrientation.alpha = data.do.alpha;
              this.initialOrientation.gamma = data.do.gamma;
            }
            let gammaAngle = data.do.gamma - this.initialOrientation.gamma;
            let betaAngle = data.do.beta - this.initialOrientation.beta;
            let maxAngle = 30;
            if(gammaAngle > maxAngle){
              this.gyroscopePositionX = 1
            }else if(gammaAngle < -maxAngle){
              this.gyroscopePositionX = -1;
            }else{
              this.gyroscopePositionX = gammaAngle / maxAngle;
            }
            if(betaAngle > maxAngle){
              this.gyroscopePositionY = 1
            }else if(betaAngle < -maxAngle){
              this.gyroscopePositionY = -1;
            }else{
              this.gyroscopePositionY = betaAngle / maxAngle;
            }
            // console.log('X :' + this.gyroscopePositionX);
            // console.log('Y :' + this.gyroscopePositionY);
          });
        }).catch(function(e){
          console.log(e);
        });
      }
    });
  }

  loopLiquify(){
    for(var i = 0; i<10; i++){
      this.liquify(this.positionLiquids[i].positionX, this.positionLiquids[i].positionY,
                  this.positionLiquids[i].positionX - this.oldPositionLiquids[i].positionX,
                  this.positionLiquids[i].positionY - this.oldPositionLiquids[i].positionY);
      this.oldPositionLiquids[i].positionX = this.positionLiquids[i].positionX;
      this.oldPositionLiquids[i].positionY = this.positionLiquids[i].positionY;
      this.positionLiquids[i].positionX += this.gyroscopePositionX*5;
      this.positionLiquids[i].positionY += this.gyroscopePositionY*5;
    }
  }

   // skew pixels based on the velocity of the mouse (dx, dy)
   liquify(x, y, dx, dy) {

    // build brush box around mouse pointer
    // var c =  this.saveCanvas[0].getContext('2d');
    var c = this.imageCanvas.nativeElement.getContext('2d');

    x = x - this.BRUSH_SIZE/2; // a arrondir ???
    y = y - this.BRUSH_SIZE/2;

    // check bounding with a defined brush dimension
    if (x < 0 ||
        y < 0 ||
        (x + this.BRUSH_SIZE) >= this.imageCanvas.nativeElement.width ||
        (y + this.BRUSH_SIZE) >= this.imageCanvas.nativeElement.height) {
          return;
        }

    var bitmap = c.getImageData(x, y, this.BRUSH_SIZE, this.BRUSH_SIZE);

    // note - each pixel is 4 bytes in byte array bitmap.data

    // bound dx, dy within brush size
    dx = (dx > 0) ? ~~Math.min(bitmap.width/2, dx) : ~~Math.max(-bitmap.width/2, dx);
    dy = (dy > 0) ? ~~Math.min(bitmap.height/2, dy) : ~~Math.max(-bitmap.height/2, dy);

    var buffer = c.createImageData(bitmap.width, bitmap.height),
        d = bitmap.data,
        _d = buffer.data,
        bit = 0;  // running bitmap index on buffer

      for(var row = 0; row < bitmap.height; row++) {
        for(var col = 0; col < bitmap.width; col++) {

          // distance from center gives intensity of smear
          var xd = bitmap.width/2 - col,
              yd = bitmap.height/2 - row,
              dist = Math.sqrt(xd*xd + yd*yd),

              x_liquify = (bitmap.width-dist)/bitmap.width,
              y_liquify = (bitmap.height-dist)/bitmap.height,

          // make intensity fall off cubic (x^3)
              skewX = (dist > this.SMUDGE_SIZE/2) ? -dx * x_liquify*x_liquify*x_liquify : -dx,
              skewY = (dist > this.SMUDGE_SIZE/2) ? -dy * y_liquify*y_liquify*y_liquify : -dy;

              var fromX = col + skewX;
              var fromY = row + skewY;

          if (fromX < 0 || fromX > bitmap.width) {
            fromX = col;
          }

          if (fromY < 0 || fromY > bitmap.height) {
            fromY = row;
          }

          // origin bitmap index on bitmap
          var o_bit = ~~fromX * 4 +  ~~fromY * bitmap.width * 4;

          // not quite sure why this occasionally is undefined
          if (d[o_bit] === undefined) {
            o_bit = bit;
          }

          _d[bit]     = this.applyContrast(d[bit], d[o_bit]);         // r
          _d[bit + 1] = this.applyContrast(d[bit + 1], d[o_bit + 1]); // g
          _d[bit + 2] = this.applyContrast(d[bit + 2], d[o_bit + 2]); // b
          _d[bit + 3] = this.applyContrast(d[bit + 3], d[o_bit + 3]); // a

          bit += 4;
        }
      }

    try {
      this.imageCanvas.nativeElement.getContext('2d').putImageData(buffer, x, y);
    } catch(e) {
    }
  }

  applyContrast(o, n) {
    return ~~((1-this.LIQUIFY_CONTRAST) * o + this.LIQUIFY_CONTRAST * n);
  }

  SMUDGE_SIZE : number = 15;
  BRUSH_SIZE : number = 50;
  LIQUIFY_CONTRAST : number = 0.9; // 0 to 1
}

interface positionLiquid  {
  positionX : number;
  positionY : number
}
