import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule} from '@ngx-translate/core';

import { Scene5Page } from './scene5';

@NgModule({
  declarations: [
    Scene5Page,
  ],
  imports: [
    IonicPageModule.forChild(Scene5Page),
    TranslateModule
  ],
})
export class Scene5PageModule {}
