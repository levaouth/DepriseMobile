import { Component, ViewChild, ElementRef } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { Scene1Page } from '../scene1/scene1';
import { TranslateService } from '@ngx-translate/core';
import { Gesture } from 'ionic-angular/gestures/gesture';

declare var Hammer: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  @ViewChild('audio') audio: ElementRef;
  @ViewChild('startLink', {read: ElementRef}) startLink: ElementRef;

  translationKey: string = 'HOME';
  ambianceTitrePath = 'assets/sound/home/ambianceTitreSoft.mp3';

  startLinkGesture: Gesture;

  constructor(public platform: Platform, public navCtrl: NavController,
              public menuCtrl: MenuController, public translateService: TranslateService) {}

  ionViewDidEnter() {
    this.audio.nativeElement.currentTime = 0;
    this.audio.nativeElement.play();

    this.startLinkGesture = new Gesture(this.startLink.nativeElement, {
      recognizers: [
        [Hammer.Swipe, { direction: Hammer.DIRECTION_HORIZONTAL, velocity: 0.01}],
        [Hammer.Tap, {taps: 1}]
      ]
    });
    this.startLinkGesture.listen();

    this.startLinkGesture.on('swipeleft tap', () => {
      this.start();
    });
  }

  start() {
    this.audio.nativeElement.pause();
    this.navCtrl.push(Scene1Page);
  }

  ngOnDestroy() {
    this.startLinkGesture.destroy();
  }
}
