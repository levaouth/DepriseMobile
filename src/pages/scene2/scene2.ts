import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { Observable, Subscription } from 'rxjs/Rx';
import { TranslateService } from '@ngx-translate/core';
import { Gesture } from 'ionic-angular/gestures/gesture';

import { ShuffleProvider } from '../../providers/shuffle/shuffle';
import { NextSceneProvider } from '../../providers/next-scene/next-scene';
import {SwipeFeedbackProvider} from '../../providers/swipe-feedback/swipe-feedback';
import { LaunchaudioProvider } from '../../providers/launchaudio/launchaudio';
import { Flatterie } from '../../app/flatterie';
declare var Hammer: any;
@IonicPage()
@Component({
  selector: 'page-scene2',
  templateUrl: 'scene2.html',
})

// 2
// Mais le rendez-vous était biaisé.
// Je ne m'en suis rendu compte que beaucoup plus tard.
// La femme en face de moi, qui paraissait si parfaite, me laissait bouche bée.
// Impossible de prononcer quelque chose de cohérent.             ==> descente de la phrase en bas de l'écran
// Sa présence me bouleversait...
// Il fallait que je pose des questions pour la mettre à jour.    ==> Debut affichage photo
// Sans que je m'en aperçoive, cette inconnue devenait ma femme.
// On a tout partagé.
// Mais jamais je ne suis parvenu à vraiment la connaître.
// Aujourd'hui encore je me pose des questions.
// Qui d'elle ou moi suit l'autre ?
// Quand je l'aime, elle me sème.

export class Scene2Page {
  @ViewChild('textContainer', {read: ElementRef}) textContainer: ElementRef;
  @ViewChild('flatteriesGrid', {read: ElementRef}) flatteriesGrid: ElementRef;
  @ViewChild('femmeCanvas', {read: ElementRef}) femmeCanvas: ElementRef;
  @ViewChild('femmeImage', {read: ElementRef}) femmeImage: ElementRef;
  @ViewChild('fondSonoreScene2') fondSonoreScene2: ElementRef;
  @ViewChild('feedBackSwipeCanvas', {read: ElementRef}) feedBackSwipeCanvas: ElementRef;
  @ViewChild('cadre', {read: ElementRef}) cadre: ElementRef;

  //@ViewChild('audio') audio: ElementRef;

  //fondSonoreScene2:HTMLAudioElement = new Audio();
  translationKey: string = 'SCENE_2';
  translationKeyFlatterie: string = 'SCENE_2_FLATTERIES';

  textContainerClass: string;
  phraseIndex: number;
  nbPhrases: number = 13;

  sons: string[] = ['assets/sound/scene2/public_fond_sonore_01.mp3'];
  currentSound=this.sons[0];
  soundPath: string = 'assets/sound/scene2/';
  //currentSound:string;

  public flatteries: Flatterie[] = [
    {
      phraseNormale: "Et vous travaillez dans quoi ?",
      phraseTravers: "Et vous travaillez l'envoi ?",
      son: "flatterie_1.mp3"
    },
    {
      phraseNormale: "Vous habitez la région depuis longtemps ?",
      phraseTravers: "Vous évitez la légion depuis longtemps ?",
      son: "flatterie_2.mp3"
    },
    {
      phraseNormale: "Je vous trouve vraiment très jolie !",
      phraseTravers: "Chevaux, brousse, bêlement... près jolis",
      son: "flatterie_3.mp3"
    },
    {
      phraseNormale: "J'ai l'impression qu'on a beaucoup de points communs",
      phraseTravers: "Chemins pression en Allemagne point comme un...",
      son: "flatterie_4.mp3"
    },
    {
      phraseNormale: "Vous avez des yeux somptueux",
      phraseTravers: "Vous avouez des notions de tueurs",
      son: "flatterie_5.mp3"
    },
    {
      phraseNormale: "Vous venez souvent ici ?",
      phraseTravers: "Vous avez l'absent acquis",
      son:  "flatterie_6.mp3"
    },
    {
      phraseNormale: "J'aime votre façon de sourire",
      phraseTravers: "Gêne, votre garçon mourir",
      son:  "flatterie_7.mp3"
    },
    {
      phraseNormale: "Vous voulez marcher un peu ?",
      phraseTravers: "Nouveaux-nés barges et il pleut",
      son: "flatterie_8.mp3"
    },
    {
      phraseNormale: "Puis-je vous offrir un autre verre ?",
      phraseTravers: "Pigeon ouïr un Notre Père ?",
      son:  "flatterie_9.mp3"
    }
  ]


  //Nb de flatteries qui seront affichées
  //Modifier cette variables UNIQUEMENT si on souhaite afficher nbFlatterieDisplay flatteries à l'écran
  nbFlatterieDisplay: number = 5;


  flatteriesDisplayed: string[] = [];
  flatterieClassList: string[] = [];
  flatterieTextList: string[] = [];
  flatteriesDone: boolean[] = [];

  flatterieChosen: number[] = [];
  flatterieAudio: HTMLAudioElement = new Audio();

  screenWidth: number;
  screenHeight: number;

  imagesWoman : string[] = [
    "assets/imgs/femme.jpg",
    "assets/imgs/femme2.jpg",
    "assets/imgs/femme3.jpg",
    "assets/imgs/femme4.jpg"
  ];
  srcFemme: string = this.imagesWoman[0];
  srcFemmeIOS : string = "assets/imgs/femmeIOS.jpg";
  NB_QUESTION_A_AFFICHER: number = 400;
  nbQuestionAffichee: number = 0;
  questions:  string[] = [
    "Qui êtes vous ?",
    "Vous aimez...",
    "Que pensez vous ?",
    "D'où venez vous ?",
    "Où allez vous ?",
    "Vous pensez quoi de..."
  ];
  //tableau des questions qui vont colorier le portrait de la femme, initialise en ionViewDidLoad
  //questions: string[6];

  textContainerGesture: Gesture;
  femmeCanvasGesture: Gesture;

  originalCanvasHeight: number = 500;
  originalCanvasWidth: number = 334;

  timer;
  style;
  sub: Subscription;
  lastSwipeTime: number = 0;  // cf swipe() : permet d'empêcher de prendre en compte plusieurs event swipe lors d'un seul mouvement.

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public swipeFeedback : SwipeFeedbackProvider,
              public shuffleProvider: ShuffleProvider, public translateService: TranslateService, public nextSceneProvider: NextSceneProvider, public launchaudioProvider: LaunchaudioProvider) {}

  ionViewDidLoad() {

    // conditions initiales
    this.phraseIndex = 0;
    this.textContainerClass = 'num-chapitre';
    this.screenWidth = window.innerWidth;
    this.screenHeight = window.innerHeight;


    this.textContainerGesture = new Gesture(this.textContainer.nativeElement, {
      recognizers: [
        [Hammer.Swipe, {direction: Hammer.DIRECTION_ALL, velocity: 0.01}],
        [Hammer.Tap, {taps: 1}]
      ]
    });
    this.textContainerGesture.listen();
    this.femmeCanvasGesture = new Gesture(this.femmeCanvas.nativeElement, {
      recognizers: [
        [Hammer.Pan, {direction: Hammer.DIRECTION_ALL}]
      ]
    });
    this.femmeCanvasGesture.listen();
    this.computeCanvasSize();
    // démarrage du timer
    this.timer = Observable.timer(0, 1000);
    this.sub = this.timer.subscribe((t)=>
      this.tickerFunc(t)
    );

    if(this.screenHeight <= 700)
      this.nbFlatterieDisplay = 3;
    else if(this.screenHeight > 700 && this.screenHeight <= 900)
      this.nbFlatterieDisplay = 4;
    else if(this.screenHeight > 900 && this.screenHeight <= 1100)
      this.nbFlatterieDisplay = 5;
    else
      this.nbFlatterieDisplay = 6;


    if(!this.platform.is('android')){
      this.srcFemme = this.srcFemmeIOS;
    }
  }

  managePhraseEvents() {
    if (this.phraseIndex == 12) {  // Fin de scène
      this.nextSceneProvider.nextScene("Scene3Page");
    }
    else{
      this.shuffle();
    }
  }

  tickerFunc(tick) {
    if(tick == 1){
      //this.currentSound = this.sons[0];
      //lancement du fond fondSonoreScene2
      //lancement du bruit de fond sonore
      this.fondSonoreScene2.nativeElement.currentTime = 0;
      //this.fondSonoreScene2.src=this.currentSound;
      //this.fondSonoreScene2.currentTime=0;
      //while (this.fondSonoreScene2.nativeElement.volume!=0.4){
      this.fondSonoreScene2.nativeElement.volume=0.5;
      //this.fondSonoreScene2.volume=0;
      //}
      console.log("VOLUME AUDIO " + this.fondSonoreScene2.nativeElement.volume);
      this.fondSonoreScene2.nativeElement.play();
      //this.fondSonoreScene2.play();
    }   

    if (tick == 3) {
      this.textContainerClass = 'text';
      this.phraseIndex++;         // Mais le rdv était biaisé
      //this.sub.unsubscribe();
      // ajout du listener sur le swipe
      this.swipeFeedback.initFeedback(this.feedBackSwipeCanvas.nativeElement, this.cadre);
      this.swipeFeedback.startFeedback();
      this.textContainerGesture.on('swipe', () => {
        this.managePhraseEvents();
        //this.shuffle();
      });
    }
  }

  shuffle() {
    if (this.phraseIndex < this.nbPhrases - 1) {
      let now = Date.now() / 1000;
      if (now - this.lastSwipeTime > 0.5) {
        this.lastSwipeTime = now;
        this.translateService.get(this.translationKey + '.phrases.ph' + ++this.phraseIndex).subscribe((translation: string) => {
          this.shuffleProvider.shuffle(this.textContainer, translation);
          console.log(translation);
        });
      }
    }
    if(this.phraseIndex == 4){
      //managePhraseEvents
      this.textContainerGesture.off('swipe', () => {
        this.managePhraseEvents();
      });
      //Deplacement de "Impossible de prononcer quelque chose de coherent" vers le bas
      this.textContainerClass = "text bottomTransition";
      setTimeout(() => { this.displayFlatterie(); }, 2000);
    }
    //Solution temporaire pour un bug : le texte disparais au moment du swipe
    if (this.phraseIndex == 5) {
      this.textContainerClass = "text bottomTransition";
    }
    if (this.phraseIndex == 6) {
      //remplissage du tableau de questions
      //on le fait maintenant et pas plus tôt au cas ou l'utilisateur change de langue au cours de la scene
      for (var i=0;i<6;i++){
        this.translateService.get(this.translationKey + '.questions.q' + (i+1)).subscribe((translation: string) => {
          //console.log(translation);
          this.questions[i]=translation;
        });
      }
      this.femmeCanvasGesture.on('pan', (event) => {
        this.createTextOnTouchMove(event)
      });
      this.swipeFeedback.stopFeedback();
      this.textContainerGesture.off('swipe', () => {
        this.managePhraseEvents();
      });
      setTimeout(()=>{ this.showOneQuestion(); }, 100);
    }
    if(this.phraseIndex == 12){ //Fin de scene
      this.swipeFeedback.endFeedback();
      this.textContainerGesture.off('swipe', () => {
        this.managePhraseEvents();
      });
      this.textContainerGesture.on('tap', () => {
        //arrêt du bruit de fond sonore avant de lancer la scene suivante
        this.fondSonoreScene2.nativeElement.pause();
        //this.fondSonoreScene2.pause();
        this.managePhraseEvents();
      });
      //chgt de classe
      this.textContainerClass = "textFinScene bottomTransition";
    }
  }

  public displayFlatterie(){
    this.swipeFeedback.stopFeedback();
    //Initialisation des nbFlatterieDisplay flatteries affichées
    for (let i:number = 0 ; i < this.nbFlatterieDisplay ; i++) {
      this.flatteriesDisplayed.push("flatterie"+i);
      this.flatterieClassList.push("text texteCliquable noDisplay spaceBeetween");
      this.flatterieTextList.push("");
      this.flatteriesDone.push(false);
    }

    while(this.flatterieChosen.length < this.nbFlatterieDisplay){
      let randomnumber = Math.floor(Math.random()*9);
      if(this.flatterieChosen.indexOf(randomnumber) > -1) continue;
      this.flatterieChosen[this.flatterieChosen.length] = randomnumber;
    }

    for (let i in this.flatteriesDisplayed) {
      this.flatterieTextList[i] = this.flatteries[this.flatterieChosen[i]].phraseNormale;
      this.flatterieClassList[i] = "texteCliquable flatterie spaceBeetween";
   }

    this.textContainerClass= "text bottomFixed";
  }

  touch(index : number){
    if(!this.flatteriesDone[index]){
      this.translateService.get(this.translationKeyFlatterie + '.' + this.flatteries[this.flatterieChosen[index]].phraseTravers).subscribe((translation: string) => {
        let flatterie = document.getElementById("flatterie"+index);
        this.shuffleProvider.shuffle2(flatterie, translation);
      });

      this.launchaudioProvider.launchAudio(this.flatteries[this.flatterieChosen[index]].son, this.flatterieAudio, this.soundPath, this.translateService.currentLang.toUpperCase());

      this.flatterieClassList[index] = "texteCliquable flatterie fadeOut spaceBeetween";

      this.flatteriesDone[index] = true;
    }

    //Vérifie si toutes les phrases ont été survolées
    for(let f of this.flatteriesDone){
      if(!f){
        return;
      }
    }
    //Si c'est le cas, on supprime les flatteries et on ré-autorise le swipe
    this.removeAllFlatteriesAndNextSentence();
  }

  removeAllFlatteriesAndNextSentence(){
    //attendre +2s pour que les timeouts aient fini
    setTimeout( ()=>{
      this.flatteriesGrid.nativeElement.remove();
      this.swipeFeedback.startFeedback();
      this.textContainerGesture.on('swipe', () => {
        this.managePhraseEvents();
      });
    }, 2500);
  }

  showOneQuestion(){
    let canvaPosition = this.femmeCanvas.nativeElement.getBoundingClientRect();
    let positionInCanvaX = canvaPosition.width/2;
    let positionInCanvaY = canvaPosition.height/2;
    this.createText(positionInCanvaX,positionInCanvaY,this.femmeCanvas.nativeElement);
  }

  createTextOnTouchMove(event) {
    let canvaPosition = this.femmeCanvas.nativeElement.getBoundingClientRect();
    let positionInCanvaX = event.center.x - canvaPosition.left;
    let positionInCanvaY = event.center.y - canvaPosition.top;
    this.createText(positionInCanvaX, positionInCanvaY, this.femmeCanvas.nativeElement);
  }

  createText(posX:number, posY:number, c:any){
    console.log('createText');

    if(this.platform.is('android')){
      this.srcFemme = this.getRandomImage();
      this.srcFemme = this.imagesWoman[this.nbQuestionAffichee%this.imagesWoman.length];
    }

    let ctx = c.getContext('2d');
    try {
    // des fois l'image met trop de temps à se recharger et n'existe plus au moment de l'export, dans ce cas, on ignore (1/1000 à peu près)
      ctx.font = '15px Arial';  // on choisit la police d'affichage
      ctx.textAlign = 'center';
      if(this.platform.is('android')){
        var style = ctx.createPattern(this.femmeImage.nativeElement, 'no-repeat');
        ctx.fillStyle = style;     // on choisit l'image pour remplir le texte
      }else{
        ctx.globalCompositeOperation = 'destination-out';     // le texte agit comme une gomme qui révèle l'image cachée en dessous
        ctx.fillStyle = 'rgba(0, 0, 0, 0.8)';
      }
      ctx.fillText(this.getRandomQuestion(), posX, posY);
      // ctx.fillText(this.questions[0], posX, posY);
    } catch(e) {
      console.log(e);
    }
    this.nbQuestionAffichee++; // on incrémente le nombre de phrase affichées

    if ( this.nbQuestionAffichee > this.NB_QUESTION_A_AFFICHER && this.phraseIndex == 6 ) {
      this.swipeFeedback.startFeedback();
      this.textContainerGesture.on('swipe', () => {
        this.managePhraseEvents();
      });
      this.shuffle();
    }
  }

  getRandomImage(){
    let randomInt = Math.floor(Math.random() * this.imagesWoman.length);
    return this.imagesWoman[randomInt];
  }

  getRandomQuestion() {
    // renvoie une question au hasard parmi celles qu'on peut poser
    let randomInt = Math.floor(Math.random() * this.questions.length);
    return this.questions[randomInt];
  }

  computeCanvasSize(){
    let useWidth:number = this.originalCanvasWidth;
    if(this.screenWidth < this.originalCanvasWidth) {
      useWidth = this.screenWidth*0.85;
    }
    // set femmeCanvas size
    this.femmeCanvas.nativeElement.width = useWidth;
    this.femmeCanvas.nativeElement.height = this.originalCanvasHeight * (useWidth/this.originalCanvasWidth);
    // set femme image size (same as femmeCanvas size)
    this.femmeImage.nativeElement.width = useWidth - 2;
    this.femmeImage.nativeElement.height = this.originalCanvasHeight * (useWidth/this.originalCanvasWidth) - 2;
    // hide femme image
    let ctx = this.femmeCanvas.nativeElement.getContext('2d');
    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  }

  ngOnDestroy() {
    this.textContainerGesture.destroy();
  }

  ionViewDidLeave(){
    //arreter le son et le feedback si on quitte la scene 2
    this.fondSonoreScene2.nativeElement.pause();
    this.swipeFeedback.endFeedback();
  }

}
