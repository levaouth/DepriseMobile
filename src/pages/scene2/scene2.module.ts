import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule} from '@ngx-translate/core';

import { Scene2Page } from './scene2';

@NgModule({
  declarations: [
    Scene2Page,
  ],
  imports: [
    IonicPageModule.forChild(Scene2Page),
    TranslateModule.forChild()
  ],
})

export class Scene2PageModule {}
