import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrixPage } from './prix';
import { TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [
    PrixPage,
  ],
  imports: [
    IonicPageModule.forChild(PrixPage),
    TranslateModule
  ],
})
export class PrixPageModule {}
