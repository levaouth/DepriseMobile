import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule} from '@ngx-translate/core';
import { PresentationPage } from './presentation';

@NgModule({
  declarations: [
    PresentationPage,
  ],
  imports: [
    IonicPageModule.forChild(PresentationPage),
    TranslateModule
  ],
})
export class PresentationPageModule {}
