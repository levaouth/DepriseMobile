import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';



@IonicPage()
@Component({
  selector: 'page-presentation',
  templateUrl: 'presentation.html',
})
export class PresentationPage {

  translationKey: string = 'PRESENTATION';

  constructor(public navCtrl: NavController, public navParams: NavParams, public translateService: TranslateService) {
  }

  ionViewDidLoad() {
  }

}
