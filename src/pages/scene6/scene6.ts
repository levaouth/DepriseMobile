import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable, Subscription } from 'rxjs/Rx';
import { TranslateService } from '@ngx-translate/core';
import { Gesture } from 'ionic-angular/gestures/gesture';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { ShuffleProvider } from '../../providers/shuffle/shuffle';
import { NextSceneProvider } from '../../providers/next-scene/next-scene';
declare var Hammer: any;
@IonicPage()
@Component({
  selector: 'page-scene6',
  templateUrl: 'scene6.html',
  providers: [
    ScreenOrientation
  ]
})

// 6
// Il est temps de reprendre le contrôle.
// Arrêter de tourner en rond.
// Fin

export class Scene6Page {
  @ViewChild('textContainer', {read: ElementRef}) textContainer: ElementRef;
  @ViewChild('GhostLettersCanvas', {read: ElementRef}) GhostLettersCanvas: ElementRef;
  @ViewChild('cadre', {read: ElementRef}) cadre: ElementRef;
  @ViewChild('textareaFinal', {read: ElementRef}) textareaFinal: ElementRef;

  translationKey: string = 'SCENE_6';

  textContainerClass: string
  phraseIndex: number;
  nbPhrases: number = 4;

  textContainerGesture: Gesture;
  lastSwipeTime: number = 0;  // cf swipe() : permet d'empêcher de prendre en compte plusieurs event swipe lors d'un seul mouvement.

  timer;
  sub: Subscription;

  textareaFinalClass : string = "textefinal";
  showLastText = 'none';
  displayText : string = 'inline';

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public shuffleProvider: ShuffleProvider, public translateService: TranslateService,
              public screenOrientation: ScreenOrientation,
            public nextSceneProvider: NextSceneProvider) {}

  ionViewDidLoad() {
    // conditions initiales
    this.phraseIndex = 0;
    this.textContainerClass = 'num-chapitre';
    this.lastSwipeTime = 0;
    this.textContainerGesture = new Gesture(this.textContainer.nativeElement, {
      recognizers: [
        [Hammer.Swipe, {direction: Hammer.DIRECTION_ALL, velocity: 0.01}],
        [Hammer.Tap, {taps: 1}]
      ]
    });
    this.textContainerGesture.listen();
    // démarrage du timer
    this.timer = Observable.timer(0, 1000);
    this.sub = this.timer.subscribe((t)=>
      this.tickerFunc(t)
    );
  }

  ionViewDidLeave(){
    this.sub.unsubscribe();
    this.textContainerGesture.destroy();
    this.refreshTimerSub.unsubscribe();
    this.cadreGesture.destroy();
  }

  tickerFunc(tick) {
    if (tick == 3) {
      this.displayText = 'none';
      this.textContainerClass = 'text';
      this.phraseIndex++;
      this.textContainerClass = 'text bottom';
      this.initiateGhostLetters();
      this.sub.unsubscribe();
    }
  }

  shuffle() {
    if(this.phraseIndex == 2){
      //console.log("YES");
      this.refreshTimerSub.unsubscribe();
      this.cadreGesture.destroy();
      let context = this.GhostLettersCanvas.nativeElement.getContext('2d');
      context.clearRect(0,0,this.GhostLettersCanvas.nativeElement.width,this.GhostLettersCanvas.nativeElement.height);
      this.showLastText = 'inline';
      //chargement du texte de fin (celui qui est affiche a la place du texte entre par l'utilisateur)
      //grace a initText
      this.initText();
      this.displayText = 'none';
      return;
    }
    if (this.phraseIndex < this.nbPhrases - 1) {
      let now = Date.now() / 1000;
      if (now - this.lastSwipeTime > 0.5) {
        this.lastSwipeTime = now;
        this.translateService.get(this.translationKey + '.phrases.ph' + ++this.phraseIndex).subscribe((translation: string) => {
          this.shuffleProvider.shuffle(this.textContainer, translation);
        });
      }
    }
  }

  cadreGesture : Gesture;
  SCREEN_HEIGHT : number;
  SCREEN_WIDTH : number;
  scale : number = window.devicePixelRatio;
  refreshTimer;
  refreshTimerSub;
  fps : number = 60;

  fingerPosition = {
    positionY : 0,
    positionX : 0,
    isFingerDown : false
  }

  ghostText : {
    text : string;
    indexGhostLetter : number
  }
  indexGhostText : number = -1;

  //seconds
  ghostLetterTime : number = 0.1;
  fadeOutTime : number = 2;
  newSentenceTime : number = 0.5;
  stopGhostLetter : boolean = false;

  UpdateCounter : number = 0;
  opacityHide: number = 0;

  GhostTextEnded: boolean = false;


  initiateGhostLetters(){
    this.SCREEN_HEIGHT = window.innerHeight;
    this.SCREEN_WIDTH = window.innerWidth;

    this.GhostLettersCanvas.nativeElement.height = this.SCREEN_HEIGHT * this.scale;
    this.GhostLettersCanvas.nativeElement.width = this.SCREEN_WIDTH * this.scale;

    this.screenOrientation.onChange().subscribe(
      () => {
        this.SCREEN_HEIGHT = window.innerHeight;
        this.SCREEN_WIDTH = window.innerWidth;

        this.GhostLettersCanvas.nativeElement.height = this.SCREEN_WIDTH * this.scale;
        this.GhostLettersCanvas.nativeElement.width = this.SCREEN_HEIGHT * this.scale;

        this.GhostLettersCanvas.nativeElement.getContext('2d').textAlign = 'center';
        this.GhostLettersCanvas.nativeElement.getContext('2d').font = 18*this.scale + 'px Arial';
        this.GhostLettersCanvas.nativeElement.getContext('2d').fillStyle = 'rgba(255, 255, 255, 1)';
      }
   );

    this.GhostLettersCanvas.nativeElement.getContext('2d').textAlign = 'center';
    this.GhostLettersCanvas.nativeElement.getContext('2d').font = 18*this.scale + 'px Arial';
    this.GhostLettersCanvas.nativeElement.getContext('2d').fillStyle = 'rgba(255, 255, 255, 1)';

    this.cadreGesture = new Gesture(this.cadre.nativeElement,  {
      recognizers: [
        [Hammer.Pan, {direction: Hammer.DIRECTION_ALL}],
      ]
    });
    this.cadreGesture.listen();

    this.cadreGesture.on('pan', (event) => {
      this.updateFingerPosition(event);
    });

    this.cadreGesture.on('panend', (event) => {
      this.fingerUp(event);
    });

    this.updateIndexGhostText();

    this.refreshTimer = Observable.timer(0, 1000/this.fps);
    this.refreshTimerSub = this.refreshTimer.subscribe(() =>
      this.updateCanvasLayout()
    );
  }

  updateFingerPosition(event){
    this.fingerPosition.positionX = Math.floor(event.center.x);
    this.fingerPosition.positionY = Math.floor(event.center.y);
    if(!this.fingerPosition.isFingerDown) this.fingerPosition.isFingerDown = true;
  }

  fingerUp(event){
    this.fingerPosition.isFingerDown = false;
  }

  updateCanvasLayout(){
    this.UpdateCounter++;
    if(!this.stopGhostLetter){
      if(this.fingerPosition.isFingerDown){
        if(this.UpdateCounter >= this.ghostLetterTime*this.fps){
          if(this.ghostText.text.length > ++this.ghostText.indexGhostLetter){
            let ctx : CanvasRenderingContext2D = this.GhostLettersCanvas.nativeElement.getContext('2d');
            ctx.fillStyle = 'rgba(255, 255, 255, 1)';
            ctx.fillText(this.ghostText.text.charAt(this.ghostText.indexGhostLetter),this.fingerPosition.positionX*this.scale,this.fingerPosition.positionY*this.scale);
            this.UpdateCounter = 0;
          }else{
            this.UpdateCounter = 0;
            this.stopGhostLetter = true;
          }
        }
      }
    }else {
      if(this.UpdateCounter < this.fadeOutTime*this.fps){
        this.opacityHide += 0.1/(this.fadeOutTime*this.fps);
        let ctx : CanvasRenderingContext2D = this.GhostLettersCanvas.nativeElement.getContext('2d');
        if(this.opacityHide <= 1){
          ctx.fillStyle = 'rgba(0,0,0, '+ this.opacityHide +')';
        }else{
          ctx.fillStyle = 'rgba(0,0,0,1)';
        }
        ctx.fillRect(0, 0, this.GhostLettersCanvas.nativeElement.width, this.GhostLettersCanvas.nativeElement.height);
      }else if (this.UpdateCounter < this.fadeOutTime*this.fps + this.newSentenceTime){
        return;
      }else{
        this.opacityHide = 0;
        this.stopGhostLetter = false;
        this.UpdateCounter = 0;
        this.updateIndexGhostText();
      }
    }
  }

  updateIndexGhostText(){
    this.indexGhostText++;
    if(this.indexGhostText == 1){
      this.displayText = "inline";
    }
    if(this.indexGhostText == 3 ){
      let indexPhrase : number;
      if(this.GhostTextEnded){
        indexPhrase = this.phraseIndex;
      }else{
        indexPhrase = this.phraseIndex + 1;
      }
      this.translateService.get(this.translationKey + '.phrases.ph' + indexPhrase).subscribe((translation: string) => {
        this.updateGhostText(translation);
      });
    }else if(this.indexGhostText < 3){
      this.translateService.get(this.translationKey + '.sprayText.sp' + this.indexGhostText).subscribe((translation: string) => {
        this.updateGhostText(translation);
      });
    }else if(this.indexGhostText > 3){
      if(!this.GhostTextEnded){
        this.textContainerGesture.on('swipe', () => {
            this.shuffle();
        });
        this.GhostTextEnded = true;
        this.shuffle();
      }
      this.indexGhostText = 0;
    }
  }

  updateGhostText(newText : string){
    this.ghostText = {
      text : newText,
      indexGhostLetter : -1
    }
  }



  letterCount : number = 0;
  textEnd : string ;
  nbSentenceTextEnd : number = 5;
  inputSoundSrc : string = "assets/sound/scene6/keyShort.mp3";
  // inputAudio : HTMLAudioElement[] = [];

  initText(){
    this.audio.src = this.inputSoundSrc;
    this.textEnd = "";
    for(let i = 0; i < this.nbSentenceTextEnd; i++){
      this.translateService.get(this.translationKey + '.endText.ph' + i).subscribe((translation: string) => {
        this.textEnd += translation;
        if(i < this.nbSentenceTextEnd-1)
          this.textEnd += "\n";
      });
    }
    //console.log(this.textEnd);
  }

  preventTexteareaDefaultBehaviour : boolean = true;

  updateTextArea(event :KeyboardEvent){
    if(this.preventTexteareaDefaultBehaviour){
      event.preventDefault();
      event.stopPropagation();
    }
    let textarea : HTMLTextAreaElement = this.textareaFinal.nativeElement;
    let text : string = textarea.value;
    if (this.letterCount<this.textEnd.length){
      textarea.value =  text.substr(0, this.letterCount) + this.textEnd.charAt(this.letterCount);
      this.letterCount++;
    } else if(this.preventTexteareaDefaultBehaviour){
      this.inputSoundSrc = "assets/sound/scene6/carretReturn.mp3";
      this.audio.src = this.inputSoundSrc;
      this.preventTexteareaDefaultBehaviour = false;
      this.textareaFinalClass = "textefinal fadeOut";
      setTimeout(() => {
        this.launchEnd();
      }, 4100);
    }
  }

  audio : HTMLAudioElement = new Audio();  

  playSound(event : KeyboardEvent){
    if(this.preventTexteareaDefaultBehaviour){
      event.preventDefault();
      event.stopPropagation();
    }
    // let audio = new Audio();
    this.audio.play();
  }

  preventNormalBehaviour(event : KeyboardEvent){
    if(this.preventTexteareaDefaultBehaviour){
      event.preventDefault();
      event.stopPropagation();
    }
  }

  //Affichage du mot "fin" qui clot l'experience utilisateur
  launchEnd(){
    //console.log("DISIZ THE END");
    this.showLastText = "none";
    this.displayText = "inline";
    this.textContainerClass = "textFinScene large";
    this.translateService.get(this.translationKey + '.phrases.ph' + ++this.phraseIndex).subscribe((translation: string) => {
      this.textContainer.nativeElement.innerHTML = translation;
    });
    this.textContainerGesture.on('tap', () => {
      //POUR PASSER A LA HOME PAGE SUR UN TAP SUR LE MOT DE FIN 
      console.log("ENTREE DANS LE TAP")
      this.nextSceneProvider.nextScene("HomePage");
    });
  }


}
