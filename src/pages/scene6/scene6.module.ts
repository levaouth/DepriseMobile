import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule} from '@ngx-translate/core';

import { Scene6Page } from './scene6';

@NgModule({
  declarations: [
    Scene6Page,
  ],
  imports: [
    IonicPageModule.forChild(Scene6Page),
    TranslateModule
  ],
})
export class Scene6PageModule {}
