import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule} from '@ngx-translate/core';

import { Scene1Page } from './scene1';

@NgModule({
  declarations: [
    Scene1Page
  ],
  imports: [
    IonicPageModule.forChild(Scene1Page),
    TranslateModule
  ]
})

export class Scene1PageModule {}
