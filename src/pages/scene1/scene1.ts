import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { Observable, Subscription } from 'rxjs/Rx';
import { TranslateService } from '@ngx-translate/core';
import { Gesture } from 'ionic-angular/gestures/gesture';
import { Particle } from '../../app/particle';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { ShuffleProvider } from '../../providers/shuffle/shuffle';
import { SwipeFeedbackProvider } from '../../providers/swipe-feedback/swipe-feedback';
import { NextSceneProvider } from '../../providers/next-scene/next-scene';
import { GyroNorm } from '../../gyronorm.complete';


declare var Hammer: any;

@IonicPage({segment: 'scene1'})
@Component({
  selector: 'page-scene1',
  templateUrl: 'scene1.html',
  providers: [
    ScreenOrientation
  ]
})

// 0  Avez-vous pensé à allumer vos enceintes ?
// 1  1
// 2
// 3  Toute ma vie, j'ai cru avoir devant moi un champ des possibles infini.
// 4  "L'univers entier m'appartient", pensais-je.
// 5  J'ai le choix.
// 6  Je suis maître de mon destin.      ==> tap
// 7  Je peux prendre ce qui me plaît.
// 8  Je deviendrai ce que je veux.
// 9  J'ai tracé mon propre chemin.
// 10 J'ai parcouru de magnifiques paysages.
// 11 Quoi de plus naturel, je les avais choisis.
// 12 Mais depuis un moment, j'ai des doutes.
// 13 Comment avoir prise sur ce qui m'arrive ?
// 14 Tout s'échappe.
// 15 Me glisse entre les doigts.
// 16 Les objets, les personnes.
// 17 J'ai l'impression de ne plus rien contrôler.
// 18 Depuis quelque temps maintenant,
// 19 Je n'attends qu'une chose
// 20 La suite.
// 21 Et le rendez-vous est arrivé.

export class Scene1Page {
  @ViewChild('textContainer', {read: ElementRef}) textContainer: ElementRef;
  @ViewChild('inputTel', {read: ElementRef}) inputTel: ElementRef;
  @ViewChild('cadre', {read: ElementRef}) cadre: ElementRef;
  @ViewChild('particlesCanvas', {read: ElementRef}) particlesCanvas: ElementRef;
  @ViewChild('feedBackSwipeCanvas', {read: ElementRef}) feedBackSwipeCanvas: ElementRef;

  translationKey: string = 'SCENE_1';
  textContainerClass: string;
  phraseIndex: number = 0;
  nbPhrases: number = 22;

  displaySwipeIndication: string = "none";
  timeoutSwipeIndication: number;

  soundPath: string = 'assets/sound/scene1/';
  sons: string[] = ['BienvenueAppuyez.mp3',
           'BipEtBravo.mp3',
           'BonjourSiVousVoulezQueVotreRendezVous.mp3',
           'BravoVotreRendezVousEstArrive.mp3'
  ];

  notesMusique = ['notesMusique/n1.mp3',
                  'notesMusique/n2.mp3',
                  'notesMusique/n3.mp3',
                  'notesMusique/n4.mp3',
                  'notesMusique/n5.mp3',
                  'notesMusique/n6.mp3',
                  'notesMusique/n7.mp3',
                  'notesMusique/n8.mp3',
                  'notesMusique/n9.mp3',
                  'notesMusique/n10.mp3',
                  'notesMusique/n11.mp3',
                  'notesMusique/n12.mp3',
                  'notesMusique/n13.mp3',
                  'notesMusique/n14.mp3',
                  'notesMusique/n15.mp3',
                  'notesMusique/n16.mp3',
                  'notesMusique/n17.mp3',
                  'notesMusique/n18.mp3',
                  'notesMusique/n19.mp3',
                  'notesMusique/n20.mp3',
                  'notesMusique/n21.mp3',
                  'notesMusique/n22.mp3'
  ];

  soundTags: string[] = [null, null, null, null, null];
  lastSoundTime: number = 0;

  particleSizes: number[] = [15, 30, 60]     // taille des points en pixel
  particleColors: string[] =
    [
      'rgb(143, 201, 255)',     // bleu
      '#74fad8',
      'rgb(45, 25, 228)',
      'rgb(255, 152, 121)',     // rouge
      '#ff3939',
      'rgb(233, 12, 12)',
      'rgb(161, 227, 112)',     // vert
      '#79c561',
      'rgb(20, 238, 49)',
      'rgb(252, 249, 153)',     // jaune
      '#f4ff4c',
      'rgb(214, 248, 20)',
      'rgb(255, 217, 238)',     // violet
      '#ff2dd2',
      'rgb(161, 2, 153)',
      'rgb(182, 102, 102)',     // rose
      '#fa7487',
      'rgb(253, 54, 164)',
      'rgb(248, 185, 108)',     // marron
      '#e09d39',
      'rgb(97, 36, 0)',
      'rgb(255, 190, 73)',      // orange
      '#f1b864',
      'rgb(255, 117, 38)'
    ]


  timer;            // 1000 --> cf tickerFunc() pour le début de la scène
  timerSub: Subscription;
  explosionTimer;   // 1500 --> change la direction de déplacement des particles
  explosionTimerSub: Subscription;
  trickleTimer;     // 1000/15 --> lorsque le gyroscope est activé
  trickleTimerSub: Subscription;

  particles: Particle[] = [];
  particlesPosition: any[] = [];
  lastParticleIndex: number;
  nbParticles: number = 0;
  nbMinParticlesToExplode: number = 25;
  nbMaxParticles: number = 50;

  gyroNorm : any;
  initialOrientation = {
    alpha : 0,
    beta : 0,
    gamma : 0
  };
  gyroscopePositionX : number = 0;
  gyroscopePositionY : number = 0;

  subKeyPressed: Subscription;
  screenTouch: Subscription;
  lastSwipeTime: number;        // cf shuffle() : permet d'empêcher de prendre en compte plusieurs event swipe lors d'un seul mouvement.
  textContainerGesture: Gesture;
  cadreGesture: Gesture;
  SCREEN_WIDTH: number = window.innerWidth;
  SCREEN_HEIGHT: number = window.innerHeight;

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform,
              public screenOrientation: ScreenOrientation,
              public shuffleProvider: ShuffleProvider, public nextSceneProvider: NextSceneProvider,
              public translateService: TranslateService, public swipeFeedback : SwipeFeedbackProvider) {}

  fingerAvoided:boolean = false;
  /**
  * Associe une position à atteindre pour chacune des lettres
  */
  avoidFinger(event) {
    let posX = event.center.x;
    let posY = event.center.y;
    // pour chaque lettre de this.lettersToDraw, si sa position est dans le cercle de event,
    // bouger la lettre
    this.lettersToDraw.forEach(letter => {
      if (letter.position.y > posY - 10 && letter.position.y < posY + 10) {
        if (letter.position.x < posX + 10 && letter.position.x > posX - 10) {
          this.fingerAvoided = true;
          // donner une position aux lettres qui doivent se déplacer.
          let diffX = Math.abs(letter.position.x - posX);
          let direction = Math.random();
          if (direction > 0.5) {
            letter.positionToReach.y -= 40 - diffX;
          } else {
            letter.positionToReach.y += 40 - diffX;
          }
          if (letter.position.x < posX) {
            letter.positionToReach.x -= this.getRandomInt(15)*diffX;
          } else {
            letter.positionToReach.x += this.getRandomInt(15)*diffX;
          }

          setTimeout(() => {
            letter.positionToReach.x = letter.originalPosition.x;
            letter.positionToReach.y = letter.originalPosition.y;
            setTimeout(() => {
              this.translateService.get(this.translationKey + '.phrases.ph' + this.phraseIndex).subscribe((translation: string) => {
                this.textContainer.nativeElement.innerHTML = translation;
                this.lettersToDraw = [];
              });
            }, 2000);
          }, 2000);
        }
      }
    });
  }

  moveLetters() {
    this.lettersToDraw.forEach(letter => {
      if (letter.position.x !== letter.positionToReach.x) {
        // reach new position with some lag
        letter.shift.x += (letter.positionToReach.x - letter.shift.x) * (letter.speed);
        letter.shift.y += (letter.positionToReach.y - letter.shift.y) * (letter.speed);
        // Apply position
        letter.position.x = letter.shift.x;
        letter.position.y = letter.shift.y;
      }
    });
  }

  /**
  * Efface tout ce qui est dessiné sur la canvas en le remplaçant par un écran noir.
  */
  clearCanvas() {
    let ctx = this.particlesCanvas.nativeElement.getContext('2d');
    ctx.fillStyle = 'rgba(0,0,0,0.1)';
    ctx.fillRect(0, 0, this.SCREEN_WIDTH, this.SCREEN_HEIGHT);
  }

  lettersToDraw: any[] = [];
  copyTextInCanvas() {
    let canvasPosition = this.particlesCanvas.nativeElement.getBoundingClientRect();
    let ctx = this.particlesCanvas.nativeElement.getContext('2d');
    let posX = canvasPosition.width/2;
    let headerHeight = this.cadre.nativeElement.firstChild.offsetTop
    let posY = (canvasPosition.height + headerHeight)/2;
    this.translateService.get(this.translationKey + '.phrases.ph' + this.phraseIndex).subscribe((translation: string) => {
      let length = 0;
      let fontSize = 18;
      for (let i=0; i<translation.length; i++) {
        let letter = translation[i];
        let letterObject = {
          content: letter,
          color: 'white',
          align: 'left',
          fontSize: fontSize + 'px',
          fontFamily: 'Arial',
          position: {
            x: posX + length,
            y: posY + fontSize/2
          },
          originalPosition: {
            x: posX + length,
            y: posY + fontSize/2
          },
          positionToReach: {
            x: posX + length,
            y: posY + fontSize/2
          },
          shift: { x: posX + length, y: posY },
          speed: 0.01 + Math.random() * 0.04
        };
        this.lettersToDraw.push(letterObject);
        ctx.textAlign = letterObject.align;
        ctx.font = letterObject.fontSize + ' ' + letterObject.fontFamily;
        length += ctx.measureText(letterObject.content).width;
      }
      // center the words on canvas
      this.lettersToDraw.forEach(letter => {
        letter.position.x -= length/2;
        letter.originalPosition.x -= length/2;
        letter.positionToReach.x -= length/2;
        letter.shift.x -= length/2;
      });
    });
  }

  /**
  * Crée une bulle et l'ajoute au tableau this.particles
  * Actualise également this.particlesPosition & this.nbParticles
  * La couleur, la taille et la note de la bulle sont choisies aléatoirement
  */
  createParticle(event) {
    let size = this.particleSizes[this.getRandomInt(this.particleSizes.length)];
    let colorIndex = this.getRandomInt(this.particleColors.length/3) * 3;

    // position de la souris lors de la génération de l'événement
    let posX = Math.floor(event.center.x);
    let posY = Math.floor(event.center.y );

    // create particle object and add it to particles array
    let particle: Particle = {
      size: size,
      position: { x: posX, y: posY },
      shift: { x: posX, y: posY },
      speed: 0.01 + Math.random() * 0.04,
      colorIndex: colorIndex,
    }

    this.drawParticle(particle);

    this.lastParticleIndex = this.nbParticles%this.nbMaxParticles;
    this.particles[this.lastParticleIndex] = particle;
    this.particlesPosition[this.lastParticleIndex] = {
      x: posX,
      y: posY
    };
    this.nbParticles++;
  }

  tapIndication: any;
  displayTapIndication() {
    let canvasPosition = this.particlesCanvas.nativeElement.getBoundingClientRect();
    let posX = canvasPosition.width/4;
    let posY = canvasPosition.height/4;
    this.tapIndication = {
      size: 25,
      position: { x: posX, y: posY },
      grow: true
    }
  }

  drawTapIndication() {
    let ctx = this.particlesCanvas.nativeElement.getContext('2d');
    if (this.tapIndication.grow) {
      this.tapIndication.size++;
      if (this.tapIndication.size >= 35) {
        this.tapIndication.grow = false;
      }
    } else {
      this.tapIndication.size --;
      if (this.tapIndication.size <= 25) {
        this.tapIndication.grow = true;
      }
    }
    ctx.beginPath();
    ctx.arc(this.tapIndication.position.x, this.tapIndication.position.y, this.tapIndication.size/2, 0, Math.PI*2, true);
    ctx.strokeStyle = 'white';
    ctx.stroke();
	}

  drawParticle(particle) {
    let ctx = this.particlesCanvas.nativeElement.getContext('2d');
    // création du gradient de couleur de la bulle
    let grd = ctx.createRadialGradient(particle.position.x, particle.position.y, particle.size/10, particle.position.x, particle.position.y, particle.size/2);
    grd.addColorStop(0, this.particleColors[particle.colorIndex]);
    grd.addColorStop(0.5, this.particleColors[particle.colorIndex+1]);
    grd.addColorStop(1, this.particleColors[particle.colorIndex+2]);
    // dessin de la bulle dans le canvas
    ctx.beginPath();
    ctx.arc(particle.position.x, particle.position.y, particle.size/2, 0, Math.PI*2, true);
    ctx.fillStyle = grd;
    ctx.fill();
  }

  drawText() {
    let ctx = this.particlesCanvas.nativeElement.getContext('2d');
    this.lettersToDraw.forEach(letter => {
      ctx.fillStyle = letter.color;
      ctx.textAlign = letter.align;
      ctx.font = letter.fontSize + ' ' + letter.fontFamily;
      ctx.fillText(letter.content, letter.position.x, letter.position.y);
    });
  }

  duplicateParticle(index: number) {
    let modelParticle = this.particles[index];
    let posX = modelParticle.position.x;
    let posY = modelParticle.position.y;
    let shiftX = modelParticle.shift.x;
    let shiftY = modelParticle.shift.y;
    let particle = {
      size: modelParticle.size,
      position: {
        x: posX,
        y: posY
      },
      shift: {
        x: shiftX,
        y: shiftY
      },
      speed: modelParticle.speed,
      colorIndex: modelParticle.colorIndex,
    }
    this.particles.push(particle);
    let x = this.particlesPosition[index].x;
    let y = this.particlesPosition[index].y;
    this.particlesPosition.push({
      x: x,
      y: y
    });
    this.drawParticle(particle);
  }

  /**
  * Retourne un nombre entier aléatoire entre 0 et max-1
  */
  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  /**
  * Permet d'initialiser le canvas pour ne pas avoir de conflit au niveau de la résolution
  * (texte flou par exemple, ou tailles différentes suivant les téléphones)
  */
  pixelRatio: number;
  initCanvas() {
    let ctx = this.particlesCanvas.nativeElement.getContext("2d");
    let dpr = window.devicePixelRatio || 1;
    let bsr = ctx.webkitBackingStorePixelRatio ||
          ctx.mozBackingStorePixelRatio ||
          ctx.msBackingStorePixelRatio ||
          ctx.oBackingStorePixelRatio ||
          ctx.backingStorePixelRatio || 1;
    this.pixelRatio = dpr / bsr;

    this.particlesCanvas.nativeElement.width = this.SCREEN_WIDTH * this.pixelRatio;
    this.particlesCanvas.nativeElement.height = this.SCREEN_HEIGHT * this.pixelRatio;
    this.particlesCanvas.nativeElement.style.width = this.SCREEN_WIDTH + "px";
    this.particlesCanvas.nativeElement.style.height = this.SCREEN_HEIGHT + "px";
    ctx.setTransform(this.pixelRatio, 0, 0, this.pixelRatio, 0, 0);
  }

  /**
  * Pose les conditions initiales du gyroscope permettant les dégoulinures des bulles
  */
  initTrickle() {
    if (this.platform.is('cordova')) {
      this.gyroNorm = new GyroNorm();
      var args = {
        frequency: 300				// ( How often the object sends the values - milliseconds )
      };
      this.gyroNorm.init(args).then(() => {
        this.gyroNorm.start((data) => {
          let gammaAngle = data.do.gamma - this.initialOrientation.gamma;
          let betaAngle = data.do.beta - this.initialOrientation.beta;
          let maxAngle = 30;
          if (gammaAngle > maxAngle) {
            this.gyroscopePositionX = 1
          } else if (gammaAngle < -maxAngle) {
            this.gyroscopePositionX = -1;
          } else {
            this.gyroscopePositionX = gammaAngle / maxAngle;
          }
          if (betaAngle > maxAngle) {
            this.gyroscopePositionY = 1
          } else if (betaAngle < -maxAngle) {
            this.gyroscopePositionY = -1;
          } else {
            this.gyroscopePositionY = betaAngle / maxAngle;
          }
        });
      }).catch(function(e){
        console.log(e);
      });
    }
  }

  ionViewDidLoad() {
    // conditions initales
    this.textContainerClass = 'text';
    this.phraseIndex = 0;
    this.lastSwipeTime = 0;
    this.textContainerGesture = new Gesture(this.textContainer.nativeElement, {
      recognizers: [
        [Hammer.Swipe, {direction: Hammer.DIRECTION_ALL, velocity: 0.01}],
        [Hammer.Tap, {taps: 1}]
      ]
    });
    this.textContainerGesture.listen();
    this.cadreGesture = new Gesture(this.cadre.nativeElement,  {
      recognizers: [
        [Hammer.Pan, {direction: Hammer.DIRECTION_ALL}],
        [Hammer.Tap, {taps: 1, time: 3000}]
      ]
    });
    this.cadreGesture.listen();
    this.initCanvas();
    // démarrage du timer
    this.timer = Observable.timer(0, 1000);
    this.timerSub = this.timer.subscribe((t) =>
      this.tickerFunc(t)
    );
    // verouillage en mode portrait
    if (this.platform.is('cordova')) {
      this.platform.ready().then(() => {
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      });
    }
  }

  ionViewWillLeave() {
    let audioTags = document.getElementsByTagName('audio');
    for (let i=0; i<this.soundTags.length; i++) {
      audioTags[i].pause();
    }
    this.soundTags.forEach(soundTag => {
      soundTag = null;
    });
    this.swipeFeedback.endFeedback();
    if (this.platform.is('cordova')) {
      this.platform.ready().then(() => {
        this.screenOrientation.unlock();
      });
    }
  }

  /**
  * Appelé à chaque fois que this.phraseIndex change (à partir de this.phraseIndex == 4)
  * Gère le déclenchement des différents événements devant être catchés durant la scène
  * et le lancement des différentes animations.
  */
  managePhraseEvents() {
    if (this.phraseIndex == 4) {
      this.timerSub.unsubscribe();
      clearTimeout(this.timeoutSwipeIndication);
      this.displaySwipeIndication = "none";
    }
    // "Je suis maître de mon destin"
    if (this.phraseIndex == 6) {
      //arrêt du feedback visuel sur le swipe
      this.swipeFeedback.stopFeedback();
      // suppression du listener sur le swipe
      this.textContainerGesture.off('swipe', () => {
        this.shuffle();
      });
      // ajout du listener sur le tap sur le cadre pour générer les points de couleurs
      // la génération du premier point de couleur permet de passer à la phrase suivante.
      this.cadreGesture.on('tap', (event) => {
        this.createParticle(event);
        this.updateSound(event);
        this.shuffle();
      });
      // ajout du tutoriel pour le tap
      this.displayTapIndication();
      this.timer = Observable.timer(0, 1000/60);
      this.timerSub = this.timer.subscribe(() => {
        this.refreshCanvas();
      });
    }
    if (this.phraseIndex == 7) {
      // rétablissement du feedback visuel du swipe
      this.swipeFeedback.startFeedback();
      // rétablissement de la possibilité de swiper les phrases.
      this.textContainerGesture.on('swipe', () => {
        this.shuffle();
      });
      // les tap dans le cadre génèrent seulement des points de couleur mais ne permettent plus de passer à la phrase suivante.
      this.cadreGesture.off('tap', () => {
        this.shuffle();
      });
      this.cadreGesture.on('tap', (event) => {
        this.createParticle(event);
        this.updateSound(event);
      });
    }
     // "J'ai tracé mon propre chemin" --> génération de comètes au pan
    if (this.phraseIndex == 9) {
      // suppression du feedback sur le swipe pour ne pas polluer les comètes
      this.swipeFeedback.stopFeedback();
      this.cadreGesture.on('panstart', (event) => {
        this.createParticle(event);
      });
      this.cadreGesture.on('pan tap', (event) => {
        this.updateLastParticlePosition(event);
        this.updateSound(event);
      });
      this.cadreGesture.off('tap', (event) => {
        this.createParticle(event);
      });
    }
    // "J'ai parcouru de magnifiques paysages" --> trickle
    if (this.phraseIndex == 10) {
      // start gyroscope
      this.initTrickle();
      // stop comètes
      this.cadreGesture.off('pan tap', (event) => {
        this.updateLastParticlePosition(event);
      });
      // rétablissement du feedback du swipe
      this.swipeFeedback.startFeedback();
      this.trickleTimer = Observable.timer(0, 1000/15);
      this.trickleTimerSub = this.trickleTimer.subscribe(() => {
        this.trickle();
      });
    }
    // "Comment avoir prise sur ce qui m'arrive --> explosion de bulles"
    if (this.phraseIndex == 13) {
      // disable create particle
      this.cadreGesture.off('panstart', (event) => {
        this.createParticle(event);
      });
      this.trickleTimerSub.unsubscribe();
      // générer les bulles manquantes avant l'explosion
      if (this.particles.length < this.nbMinParticlesToExplode) {
        for (let i=0; i<this.nbMinParticlesToExplode-this.particles.length; i++) {
          this.duplicateParticle(i);
        }
      }
      this.explosionTimer = Observable.timer(0, 1500);
      this.explosionTimerSub = this.explosionTimer.subscribe(() => {
        this.updateParticlesPosition();
        this.updateSound({ type: 'tap'});
      });
    }
    // "Tout s'échappe"
    if (this.phraseIndex == 14) {
      // les lettres qui sont dans le rayon du touch doivent se déporter
      this.cadreGesture.on('pan', (event) => {
        this.avoidFinger(event);
      });
      this.cadreGesture.on('panend', (event) => {
        this.stopAvoidFinger();
      });
    }
    if (this.phraseIndex == 19) {
      this.particles = [];          // disparition des bulles
      this.timerSub.unsubscribe();
      this.timerSub = this.timer.subscribe(()=> {
        this.clearCanvas();
      });
      this.explosionTimerSub.unsubscribe();
    }
    // la suite
    if (this.phraseIndex == 20) {
    //fin du feedback du swipe a ce moment là
      this.swipeFeedback.stopFeedback();
      this.soundTags[0] = this.soundPath + this.translateService.currentLang.toUpperCase() + "_" + this.sons[2];     // si vous voulez que votre RDV...
      this.textContainerGesture.off('swipe', () => {
        this.shuffle();
      });
      setTimeout(() => {
        this.inputTel.nativeElement.focus();
        this.touchToMakeKeyboardFocus();
        // ajout du listener sur un appui sur une touche du clavier
        this.subKeyPressed = Observable.fromEvent(document, 'keydown').subscribe(e => {
          this.screenTouch.unsubscribe(); // on supprime le listener screenTouch (plus besoin de faire réapparaître le clavier)
          this.inputTel.nativeElement.blur();
          this.soundTags[0] = this.soundPath + this.translateService.currentLang.toUpperCase() + "_" + this.sons[this.sons.length - 1];
          this.translateService.get(this.translationKey + '.phrases.ph' + ++this.phraseIndex).subscribe((translation: string) => {
            this.textContainer.nativeElement.innerHTML = translation;
          });
          this.subKeyPressed.unsubscribe();     // on supprime le listener keydown dès qu'une touche a été appuyée.
          this.managePhraseEvents();
        });
      }, 5000);
    }
    if (this.phraseIndex == 21) {
      this.textContainerClass = "textFinScene";
      this.textContainerGesture.on('tap', () => {
        this.nextSceneProvider.nextScene('Scene2Page');
      });
    }
  }

  /**
  * Déplace la bulle d'index i pour qu'elle se rapproche de la position qu'elle doit atteindre,
  * spécifié dans this.particlesPosition[i], avec un certain délai.
  */
  moveParticle(index: number) {
    let particle = this.particles[index];
    let posX = this.particlesPosition[index].x;
    let posY = this.particlesPosition[index].y;
    // reach new position with some lag
    particle.shift.x += (posX - particle.shift.x) * (particle.speed);
    particle.shift.y += (posY - particle.shift.y) * (particle.speed);
    // Apply position
    particle.position.x = particle.shift.x;
    particle.position.y = particle.shift.y;
  }

  ngOnDestroy() {
    this.textContainerGesture.destroy();
    this.cadreGesture.destroy();
    if(this.gyroNorm && this.gyroNorm != undefined){
      this.gyroNorm.end();
    }
  }

  /**
  * Rafraichit l'affichage du canvas. Appelé tous les 1000/60ms
  * Appelle moveParticle sur les bulles à bouger et drawParticle sur toutes les bulles
  */
  lastRefresh: number = 0;
  refreshCanvas() {
    this.clearCanvas();
    // tap indication
    if (this.phraseIndex == 6) {
      let now = Date.now() / 1000;
      // to slow the animation
      if (now - this.lastRefresh > 0.05) {
        this.drawTapIndication();
        this.lastRefresh = now;
      }
    }
    // show particles on canvas
    if (this.phraseIndex > 6 && this.phraseIndex < 19) {
      // comètes
      if (this.phraseIndex == 9) {
        this.moveParticle(this.lastParticleIndex);
      }
      // trickle + explosion
      if (this.phraseIndex >= 10) {
        for (let i=0; i<this.particles.length; i++) {
          this.moveParticle(i);
        }
      }
      this.particles.forEach(particle => {
        this.drawParticle(particle);
      });
      // tout s'échappe
      if (this.phraseIndex == 14) {
        this.moveLetters();
        this.drawText();
      }
    }
  }

  /**
  * Permet de passer à la phrase suivante en utilisant shuffle-text.js
  * Appelé au swipe sur this.textContainer
  */
  shuffle() {
    if (this.phraseIndex < this.nbPhrases - 1) {
      let now = Date.now() / 1000;
      if (now - this.lastSwipeTime > 0.5) {
        this.lastSwipeTime = now;
        if (this.phraseIndex === 13) {
          this.shuffleProvider.shuffle(this.textContainer, '                         ');
          this.phraseIndex++;
          // attendre que le shuffle ait terminé avant d'afficher la nouvelle phrase dans le canvas.
          setTimeout(() => { this.copyTextInCanvas(); }, 600);
        } else {
          this.translateService.get(this.translationKey + '.phrases.ph' + ++this.phraseIndex).subscribe((translation: string) => {
            this.shuffleProvider.shuffle(this.textContainer, translation);
          });
        }
        // appelé à chaque fois que phraseIndex change
        this.managePhraseEvents();
      }
    }
  }

  stopAvoidFinger() {
    if (this.fingerAvoided) {
      this.cadreGesture.off('pan', (event) => {
        this.avoidFinger(event);
      });
      this.cadreGesture.off('panend', () => {
        this.stopAvoidFinger();
      });
    }
  }

  /**
  * Déclenché toutes les secondes par this.timer
  * Permet de gérer le début de la scène (=> "Toute ma vie j'ai cru avoir devant moi...")
  */
  tickerFunc(tick) {
    if (tick == 3) {
      this.phraseIndex++;   // 1
      this.textContainerClass = 'num-chapitre';
    }
    if (tick == 5) {
      this.phraseIndex++;   // ""
      this.textContainerClass = 'text';
      this.soundTags[0] = this.soundPath + this.translateService.currentLang.toUpperCase() + "_" + this.sons[0];
    }
    if (tick == 9) {

      //ajout du feedback visuel
      this.swipeFeedback.initFeedback(this.feedBackSwipeCanvas.nativeElement, this.cadre);
      this.swipeFeedback.startFeedback();
      this.inputTel.nativeElement.focus();
      this.touchToMakeKeyboardFocus();
      // ajout du listener sur un appui sur une touche du clavier
      this.subKeyPressed = Observable.fromEvent(document, 'keydown').subscribe(e => {
        this.screenTouch.unsubscribe();  // on supprime le listener screenTouch (plus besoin de faire réapparaître le clavier)
        this.inputTel.nativeElement.blur();
        this.soundTags[0] =  this.soundPath + this.translateService.currentLang.toUpperCase() + "_" + this.sons[1];
        this.phraseIndex++;
        this.timeoutSwipeIndication = setTimeout(() => { this.displaySwipeIndication = "inline"; }, 5000);
        // ajout du listener sur le swipe
        this.textContainerGesture.on('swipe', () => {
          this.shuffle();
        });
        this.subKeyPressed.unsubscribe();     // on supprime le listener keydown dès qu'une touche a été appuyée.
      });
    }
  }

  /**
  * Permet de faire apparaître le clavier avec un touch sur l'écran, tant qu'aucune touche du clavier n'a été appuyée.
  */
  touchToMakeKeyboardFocus() {
    this.screenTouch = Observable.fromEvent(document, 'focusout').subscribe(e => {
      if (this.subKeyPressed != null) {
        this.inputTel.nativeElement.focus();
      }
    });
  }

  /**
  * Appelé tous les 1000/15ms par this.trickleTimer.
  * Permet de mettre à jour la position des bulles en fonction de l'inclinaison du téléphone.
  */
  trickle() {
    this.particlesPosition.forEach(particlePosition => {
      particlePosition.x += Math.floor(this.gyroscopePositionX * 5);
      particlePosition.y += Math.floor(this.gyroscopePositionY * 5);
      let maxSize = this.particleSizes[this.particleSizes.length -1];
      if (particlePosition.x > this.SCREEN_WIDTH + maxSize) {
        particlePosition.x = this.SCREEN_WIDTH + maxSize;
      }
      if (particlePosition.y > this.SCREEN_HEIGHT + maxSize) {
        particlePosition.x = this.SCREEN_HEIGHT + maxSize;
      }
      if (particlePosition.x < - maxSize) {
        particlePosition.x = - maxSize;
      }
      if (particlePosition.y < - maxSize) {
        particlePosition.y = - maxSize;
      }
    });
  }

  /**
  * Actualise la position de la dernière bulle créée en fonction de la position de l'événement passé en paramètre
  * Utilisé pour la création des comètes
  */
  updateLastParticlePosition(event) {
    let x = Math.floor(event.center.x);
    let y = Math.floor(event.center.y);
    this.particlesPosition[this.lastParticleIndex] = {
      x: x,
      y: y
    };
  }

  /**
  * Appelée toutes les 1,5 secondes à partir du début de l'explosion.
  * Attribue une nouvelle position à toutes les bulles soit en fonction de la position d'un événement,
  * soit aléatoirement.
  */
  updateParticlesPosition(event = null) {
    for (let i=0; i<this.particles.length; i++) {
      let x: number, y: number;
      if (event !== null) {
        x = Math.floor(event.center.x);
        y = Math.floor(event.center.y);
      } else {
        // les particles peuvent sortir du canvas --> meilleur effet de dispersion
        x = this.getRandomInt(this.SCREEN_WIDTH + 100);
        y = this.getRandomInt(this.SCREEN_HEIGHT + 100);
      }
      this.particlesPosition[i] = {
        x: x,
        y: y
      };
    }
  }

  /**
  * Choisit aléatoirement une note parmi this.notesMusique et l'attribue à un des champs audio
  * Gère la génération des notes pendant la création des comètes.
  * L'intervalle entre les différentes notes est choisi aléatoirement entre 0.8 et 1.4s
  */
  updateSound(event) {
    let now = Date.now() / 1000;
    let interval = (this.getRandomInt(6) + 8)/10;
    if (event.type == 'tap') {    // --> on génère une note pour chaque tap
      interval = 0;
    }
    if (now - this.lastSoundTime > interval) {
      this.lastSoundTime = now;
      let soundTagIndex = this.getRandomInt(this.soundTags.length);
      let noteIndex = this.getRandomInt(this.notesMusique.length);
      let sound = this.soundPath + this.notesMusique[noteIndex];
      this.soundTags[soundTagIndex] = sound;
    }
  }
}
