import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { Observable, Subscription } from 'rxjs/Rx';
import { TranslateService } from '@ngx-translate/core';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Gesture } from 'ionic-angular/gestures/gesture';
import { Gyroscope} from '@ionic-native/gyroscope';

import { ShuffleProvider } from '../../providers/shuffle/shuffle';
import {SwipeFeedbackProvider} from '../../providers/swipe-feedback/swipe-feedback';
import { NextSceneProvider } from '../../providers/next-scene/next-scene';
import { GyroNorm } from '../../gyronorm.complete';
declare var Hammer: any;
@IonicPage()
@Component({
  selector: 'page-scene3',
  templateUrl: 'scene3.html',
  providers: [
    ScreenOrientation,
    Gyroscope
  ]
})

// 3
// Vingt ans se sont écoulés depuis notre rencontre.
// Ce matin, je me perds dans un mot qu'elle m'a laissé.
// Tout se brouille dans mon esprit.
// Je ne sais comment l'interpréter.
// Mot d'amour ou de rupture ?
// Que puis-je faire ?

export class Scene3Page {
  @ViewChild('textContainer', {read: ElementRef}) textContainer: ElementRef;
  @ViewChild('feedBackSwipeCanvas', {read: ElementRef}) feedBackSwipeCanvas: ElementRef;
  @ViewChild('poemeCanvas', {read: ElementRef}) poemeCanvas: ElementRef;
  @ViewChild('cadre', {read: ElementRef}) cadre: ElementRef;

  translationKey: string = 'SCENE_3';
  translationKeyPoeme : string = "SCENE_3_POEME";

  textContainerClass: string;
  textContainerGesture: Gesture;

  phraseIndex: number;
  nbPhrases: number = 7;

  Poeme : string[] = ["Je sais que c'est pour toi un choc",
                       "Je n'ai que de l'amour pour toi",
                       "Est un mensonge, et",
                       "Dans un couple, il y en a un qui souffre et un qui s'ennuie",
                       "Je veux que tous nos amis sachent que",
                       "Je ne veux pas rester avec toi,",
                       "Depuis le premier jour, je ne sais pas comment tu peux croire que",
                       "Je t'aime",
                       "Mon amour",
                       "A disparu",
                       "L'indifférence",
                       "Est plus vivace que jamais",
                       "Le charme de notre rencontre",
                       "S'est dissipé à présent",
                       "Et le moindre malentendu",
                       "A vaincu",
                       "Notre amour"
                      ];
  carmen : HTMLAudioElement = new Audio();
  carmen_reverse : HTMLAudioElement = new Audio();
  reverse : boolean = false;

  lastSwipeTime: number = 0;  // cf swipe() : permet d'empêcher de prendre en compte plusieurs event swipe lors d'un seul mouvement.

  tabClearTimeout: number[] = [];

  timer;
  sub: Subscription;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public shuffleProvider: ShuffleProvider, public translateService: TranslateService,
              public screenOrientation: ScreenOrientation, public platform: Platform,
              public swipeFeedback : SwipeFeedbackProvider,
              public nextSceneProvider: NextSceneProvider) {}

  ionViewDidLoad() {
    // conditions initiales
    this.phraseIndex = 0;
    this.textContainerClass = 'num-chapitre';
    this.textContainerGesture = new Gesture(this.textContainer.nativeElement, {
      recognizers: [
        [Hammer.Swipe, {direction: Hammer.DIRECTION_ALL, velocity: 0.01}],
        [Hammer.Tap, {taps: 1}]
      ]
    });
    this.textContainerGesture.listen();
    // démarrage du timer
    this.timer = Observable.timer(0, 1000);
    this.sub = this.timer.subscribe((t)=>
      this.tickerFunc(t)
    );


    this.gyroNorm = new GyroNorm();
    var args = {
      frequency:10000,					// ( How often the object sends the values - milliseconds )
    };
    this.gyroNorm.init(args);
  }

  ionViewDidLeave(){
    if(this.platform.is('cordova')){
      this.platform.ready().then(()=>{
        this.screenOrientation.unlock();
      })
    }

    this.sub.unsubscribe();

    this.carmen.pause();
    this.carmen_reverse.pause();

    this.carmen.src = "";
    this.carmen_reverse.src = "";

    //Le remove n'a l'air de rien faire (a vérifier car sinon on prends de la ram pour rien)
    this.carmen.remove();
    this.carmen_reverse.remove();

    this.swipeFeedback.endFeedback();
    if (this.cadreGesture!=undefined){
    this.cadreGesture.destroy();
    }
    if (this.textContainerGesture!=undefined){
      this.textContainerGesture.destroy();
    }

    if(this.refreshTimer && this.refreshTimer != undefined)
      this.refreshTimerSub.unsubscribe();
    if(this.gyroNorm && this.gyroNorm != undefined)
      this.gyroNorm.end();

    this.clearAllTimeout();
  }

  clearAllTimeout(){
    for(let timeout of this.tabClearTimeout){
      clearTimeout(timeout);
    }
  }

  tickerFunc(tick) {
    if (tick==1){
      //forcage du passage en paysage. Ici et pas dans ionViewDidLoad
      //pour que meme en rappelant la scene depuis le menu, la scene soit quand
      //meme deroulee en paysage
      this.ChangeScreenLandscapeOrientation();
    }
    if (tick == 3) {
      this.textContainerClass = 'text';
      this.phraseIndex++;
      this.swipeFeedback.initFeedback(this.feedBackSwipeCanvas.nativeElement, this.cadre);
      this.swipeFeedback.startFeedback();
      // ajout du listener sur le swipe
      this.textContainerGesture.on('swipe', () => {
        this.shuffle();
      });
      this.sub.unsubscribe();
    }
  }

  shuffle() {
    if (this.phraseIndex < this.nbPhrases - 1) {
      let now = Date.now() / 1000;
      if (now - this.lastSwipeTime > 0.5) {
        this.lastSwipeTime = now;
        this.translateService.get(this.translationKey + '.phrases.ph' + ++this.phraseIndex).subscribe((translation: string) => {
          this.shuffleProvider.shuffle(this.textContainer, translation);
        });
      }
    }
    if(this.phraseIndex == 5){
      // ajout du listener sur le swipe
      this.textContainerGesture.off('swipe', () => {
        this.shuffle();
      });
      this.textContainerClass = "text leftTransition";
      this.initMusic();
      this.tabClearTimeout[this.tabClearTimeout.length] = setTimeout(()=>this.initNewPoeme(),1500);
    }
    if(this.phraseIndex == 6){ //Fin de scene
      this.textContainerGesture.on('tap', () => {
        this.nextSceneProvider.nextScene("Scene4Page");
      });
      //chgt de classe
      this.textContainerClass = "textFinScene leftTransition";
    }
  }

  initMusic(){
    this.carmen_reverse.src = "assets/sound/scene3/carmen_reverse.mp3";
    this.carmen_reverse.loop = true;
    this.carmen.src = "assets/sound/scene3/carmen.mp3";
    this.carmen.playbackRate = 1;
    this.carmen.play();
    this.carmen.loop = true;
  }


  /*
  **********************************
  **        NEW CODE CANVAS       **
  **********************************
  */

  versIndex : number = -1;
  nbVers : number = 17;

  isIOS : boolean = false;

  SCREEN_WIDTH: number = window.innerWidth;
  SCREEN_HEIGHT: number = window.innerHeight;
  windowSize : number = this.SCREEN_HEIGHT > this.SCREEN_WIDTH ? this.SCREEN_WIDTH : this.SCREEN_HEIGHT;
  canvasCenter : {
    x : number,
    y : number
  } = {x:0,y:0};
  cadreGesture: Gesture;

  interligneVers : number = 0;

  fingerPosition = {
    PositionY : 0,
    isFingerDown : false
  }

  updateFingerPosition(event){
    this.fingerPosition.PositionY = Math.floor(event.center.y);
    if(!this.fingerPosition.isFingerDown) this.fingerPosition.isFingerDown = true;
  }

  fingerUp(event){
    this.fingerPosition.isFingerDown = false;
  }

  gyroNorm : any;
  initialOrientation = {
    alpha : 0,
    beta : 0,
    gamma : 0
  };

  gyroscopePosition = {
    PositionY : 0
  }

  refreshTimer;
  refreshTimerSub : Subscription;

  fps : number = 24;

  versTable : VersPoeme[] = [];

  ChangeScreenLandscapeOrientation(){
    if(this.platform.is('cordova')){
      this.platform.ready().then(()=>{
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
      })
    }
  }

  ChangeScreenPortraitOrientation(){
    if(this.platform.is('cordova')){
      this.platform.ready().then(()=>{
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      })
    }
  }

  initNewPoeme(){
    this.cadreGesture = new Gesture(this.cadre.nativeElement,  {
      recognizers: [
        [Hammer.Pan, {direction: Hammer.DIRECTION_ALL}],
      ]
    });
    this.cadreGesture.listen();
    if(this.platform.ready().then(()=>{
      if(this.platform.is('ios')){
        this.isIOS = true;
      }
    }))

    this.cadreGesture.on('pan', (event) => {
      this.updateFingerPosition(event);
    });

    this.cadreGesture.on('panend', (event) => {
      this.fingerUp(event);
    });

    this.refreshTimer = Observable.timer(0, 1000/this.fps);
    this.refreshTimerSub = this.refreshTimer.subscribe(() =>
      this.updateCanvasLayout()
    );

    this.platform.ready().then( () => {
      if (this.platform.is('cordova')){
        var args = {
          frequency:50,					// ( How often the object sends the values - milliseconds )
        };
        this.gyroNorm.init(args).then(()=>{
          this.gyroNorm.start((data)=>{
            if(this.initialOrientation.beta == 0 && this.initialOrientation.alpha == 0){
              this.initialOrientation.beta = data.do.beta;
              this.initialOrientation.alpha = data.do.alpha;
              this.initialOrientation.gamma = data.do.gamma;
            }
            let gammaAngle = data.do.gamma - this.initialOrientation.gamma;
            let maxAngle = 20;
            if(gammaAngle > maxAngle){
              this.gyroscopePosition.PositionY = 1
            }else if(gammaAngle < -maxAngle){
              this.gyroscopePosition.PositionY = -1;
            }else{
              this.gyroscopePosition.PositionY = gammaAngle / maxAngle;
            }
            // console.log('Y :' + this.gyroscopePosition.PositionY);
          });
        }).catch(function(e){
          console.log(e);
        });
      }
    });

    this.SCREEN_HEIGHT = window.innerHeight;
    this.SCREEN_WIDTH = window.innerWidth;

    if(this.SCREEN_HEIGHT > this.SCREEN_WIDTH){
      let tmp = this.SCREEN_HEIGHT;
      this.SCREEN_HEIGHT = this.SCREEN_WIDTH;
      this.SCREEN_WIDTH = tmp;
    }

    this.initCanvas();

    let canvasPosition = this.poemeCanvas.nativeElement.getBoundingClientRect();
    this.canvasCenter.x  = canvasPosition.width/2;
    this.canvasCenter.y  = canvasPosition.height/2;

    let ctx = this.poemeCanvas.nativeElement.getContext('2d')
    ctx.textAlign = 'center';
    ctx.font = '16px Arial'

    this.versTable
    this.addVersPoeme(true);
  }

  /**
  * Permet d'initialiser le canvas pour ne pas avoir de conflit au niveau de la résolution
  * (texte flou par exemple, ou tailles différentes suivant les téléphones)
  */
  pixelRatio: number;
  initCanvas() {
    let ctx = this.poemeCanvas.nativeElement.getContext("2d");
    let dpr = window.devicePixelRatio || 1;
    let bsr = ctx.webkitBackingStorePixelRatio ||
          ctx.mozBackingStorePixelRatio ||
          ctx.msBackingStorePixelRatio ||
          ctx.oBackingStorePixelRatio ||
          ctx.backingStorePixelRatio || 1;
    this.pixelRatio = dpr / bsr;

    this.poemeCanvas.nativeElement.width = this.SCREEN_WIDTH * this.pixelRatio;
    this.poemeCanvas.nativeElement.height = this.SCREEN_HEIGHT * this.pixelRatio;
    this.poemeCanvas.nativeElement.style.width = this.SCREEN_WIDTH + "px";
    this.poemeCanvas.nativeElement.style.height = this.SCREEN_HEIGHT + "px";
    ctx.setTransform(this.pixelRatio, 0, 0, this.pixelRatio, 0, 0);
  }


  updateCanvasLayout(){
    let ctx : CanvasRenderingContext2D = this.poemeCanvas.nativeElement.getContext('2d');
    ctx.clearRect(0, 0, this.poemeCanvas.nativeElement.width, this.poemeCanvas.nativeElement.height);
    if (this.fingerPosition.isFingerDown) {
      this.interligneVers = ((1-(this.fingerPosition.PositionY/(this.windowSize/2)))*-30*this.pixelRatio);
    } else {
      this.interligneVers = this.interligneVers - this.gyroscopePosition.PositionY*this.pixelRatio*1/2;
      if (this.interligneVers < - 30*this.pixelRatio) {
        this.interligneVers = - 30*this.pixelRatio;
      } else if (this.interligneVers > 30*this.pixelRatio) {
        this.interligneVers = 30*this.pixelRatio;
      }
    }
    let lastPosition = 0;
    let deleteVers : boolean = false;
    let ajoutVers : boolean = false;
    let isNotReverse : boolean = this.interligneVers/(30*this.pixelRatio) <= 0;

    if((!this.reverse && !isNotReverse) || (this.reverse && isNotReverse)){
      for(let i = Math.floor((this.versTable.length - 1 )/2); i>=0; i--){
        let text = this.versTable[this.versTable.length - 1 - i].text;
        this.versTable[this.versTable.length - 1 - i].text = this.versTable[i].text;
        this.versTable[i].text = text;
        let index = this.versTable[this.versTable.length - 1 - i].versIndex;
        this.versTable[this.versTable.length - 1 - i].versIndex = this.versTable[i].versIndex;
        this.versTable[i].versIndex = index;
      }
    }
    

    for(let i = this.versTable.length - 1; i>=0; i--){
      let vers = this.versTable[i];
      vers.positionY = this.interligneVers*vers.growValue + lastPosition;
      if(vers.growValue < 1){
        vers.growValue += 1/(3*this.fps);
        if(vers.growValue >=1){
          ajoutVers = true;
        }
      }
      lastPosition = vers.positionY;

      if(vers.isOpacityIncrease){
        if(vers.opacity < 1){
          vers.opacity += 1/(8*this.fps)
        }else{
          vers.isOpacityIncrease = false;
        }
      }else{
        if(vers.opacity > 0){
          vers.opacity -= 1/(8*this.fps)
        }else{
          deleteVers = true;
          continue;
        }
      }

      ctx.fillStyle = 'rgba(255, 255, 255,'+ vers.opacity*(2-vers.opacity) +')';
      ctx.fillText(vers.text,this.canvasCenter.x, vers.positionY + this.canvasCenter.y );
    }
    let bufferd;
    if(isNotReverse){
      if(this.reverse){
        //Si la souris vient d'arriver en haut de l'ecran, on lance carmen en sens normal
        bufferd = this.carmen_reverse.currentTime;
        this.carmen.currentTime = 46 - bufferd;
        if(!this.isIOS)
        this.carmen.playbackRate = -this.interligneVers/(30*this.pixelRatio)*1.4 + 0.2;
        this.carmen.play();
        this.reverse = false;
        this.carmen_reverse.pause();
      }
      else{
        if(!this.isIOS)
        this.carmen.playbackRate = -this.interligneVers/(30*this.pixelRatio)*1.4 + 0.2;
      }
    }
    else if (!isNotReverse){
      if(!this.reverse){
        bufferd = this.carmen.currentTime;
        this.carmen_reverse.currentTime = 46 - bufferd;
        if(!this.isIOS)
        this.carmen_reverse.playbackRate = this.interligneVers/(30*this.pixelRatio)*1.4 + 0.2;
        this.carmen_reverse.play();
        this.reverse = true;
        this.carmen.pause();
      }
      else{
        if(!this.isIOS)
        this.carmen_reverse.playbackRate = this.interligneVers/(30*this.pixelRatio)*1.4 + 0.2;
      }
    }
    
    
    if(deleteVers){
      this.deleteVersPoeme();
    }
    if(ajoutVers){
      this.addVersPoeme(isNotReverse);
    }
    
  }
  
  
  addVersPoeme(isNotReverse : boolean){
    let indexVers;
    if(this.versTable.length == 0){
      indexVers = 0;
    }else if(isNotReverse){
      indexVers = this.versTable[this.versTable.length - 1].versIndex + 1;
    }else{
      indexVers = this.versTable[this.versTable.length - 1].versIndex - 1;
    }
    if(indexVers > this.nbVers - 1){
      indexVers = 0;
    }else if (indexVers < 0){
      indexVers = this.nbVers - 1;
    }
    this.translateService.get(this.translationKey + '.poeme.vers' + indexVers).subscribe((translation: string) => {
      let vers : VersPoeme = {
        text : translation,
        positionY : 0,
        opacity : 0,
        isOpacityIncrease : true,
        growValue : 0,
        versIndex : indexVers
      };
      this.versTable.splice(this.versTable.length, 1, vers);
    });
  }

  deletedVers : number = 0;
  deleteVersPoeme(){
    this.versTable.splice(0, 1);
    if(++this.deletedVers == 5){
      this.textContainerGesture.on('swipe', () => {
        this.endPoeme();
      });
    }
  }

  endPoeme(){
    this.refreshTimerSub.unsubscribe();
    this.gyroNorm.end();
    let canvas = this.poemeCanvas.nativeElement;
    canvas.getContext('2d').clearRect(0,0,canvas.width,canvas.height);
    //on repasse en portrait a la fin
    this.ChangeScreenPortraitOrientation();
    this.shuffle();
  }

}

interface VersPoeme{
  text : string,
  positionY : number,
  opacity : number,
  isOpacityIncrease : boolean,
  growValue : number,
  versIndex : number
}
