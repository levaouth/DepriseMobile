import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule} from '@ngx-translate/core';

import { Scene3Page } from './scene3';

@NgModule({
  declarations: [
    Scene3Page,
  ],
  imports: [
    IonicPageModule.forChild(Scene3Page),
    TranslateModule
  ],
})
export class Scene3PageModule {}
