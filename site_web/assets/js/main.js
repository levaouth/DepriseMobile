$(function(){

    function createAudio(){
        console.log("createAudio");
        var audio = document.createElement("AUDIO");
        var numberNote = Math.floor((Math.random() * 22) + 1);
        var path = "../assets/notesMusique/n" + numberNote + ".mp3";

        if (audio.canPlayType("audio/mpeg")) {
            console.log("dans if");
            audio.setAttribute("src", path);
        } else {
            console.log("dans else");
            audio.setAttribute("src", path);
        }
        audio.setAttribute("autoplay", true);

        document.body.appendChild(audio);
    }

    function goTo(href) {
        var posTop = $(href).offset().top - 20;
        $('html, body').animate({ scrollTop: posTop}, 200);
    }

    //nav-link
    $(".nav-link").click(function(){

        $(".nav-link").removeClass("active");
        $(this).addClass("active");
        var target = $(this).data("target");

        $("#contentZone").load(target + ".html")
        if(target == "presentation"){
            $("#dropdown-anchors").removeClass("hidden");
        }
        else{
            $("#dropdown-anchors").addClass("hidden");
        }

    });

    $(".nav-link .active").ready(function(){
        $("#contentZone").load("accueil.html", function(){
            //hover rond
            $(".rond").click(function(){
                createAudio();
            });
        });
        
    });

    //anchors
    $('body').on('click', '.goToAnchor', function() {
        var href = $(this).data("anchor");
        goTo(href);
    });

    
});