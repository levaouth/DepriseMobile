# TELECHARGEMENT APK :

* Version du 03/07 : [ICI](https://gitlab.utc.fr/levaouth/DepriseMobile/raw/master/apk_debug/Deprise-debug-03-07.apk)
* Version du 07/06 (nouvelle Scène 3): [ICI](https://gitlab.utc.fr/levaouth/DepriseMobile/raw/Scene3Reloaded/apk_debug/Deprise-debug-07-06.apk)
* Version du 04/06 : [ICI](https://gitlab.utc.fr/levaouth/DepriseMobile/raw/master/apk_debug/Deprise-debug-04-06.apk)
* Version du 31/05 : [ICI](https://gitlab.utc.fr/levaouth/DepriseMobile/raw/master/apk_debug/Deprise-debug-31-05.apk)

# IONIC CLI

* lancer le serveur sur un navigateur : `ionic serve`
* lancer le serveur sur un navigateur et afficher les simulateurs : `ionic serve --lab`

* émulateur : `ionic cordova emulate android -l -c`
  * `-l` : `--livereload`
  * `-c` : afficher les console logs dans le terminal  


* créer une nouvelle page : `ionic g page new_page_name`

* running app : `ionic cordova run android --device`

* créer un fichier .apk : `ionic cordova build android` <br />
Le fichier se trouve alors (en local) dans le dossier platforms/android/app/build/outputs/apk/debug
